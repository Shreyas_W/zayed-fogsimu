/* 
 * $Id$
 * 
 * Copyright (c) 2019-2020 Stephane Galland and Zayed University.
 * 
 * This source code is not open-source. It is the joint proprietary of Stephane Galland
 * and the Zayed University, United Arab Emirates, according to the terms of the
 * contract passed between the parties.
 */
package fr.ciadlab.fogsimu.fx.drawers

import fr.ciadlab.fogsimu.environment.FogZone
import fr.ciadlab.fogsimu.preferences.FogPreferenceManager
import javafx.scene.paint.Color
import org.arakhne.afc.gis.ui.drawers.AbstractMapCircleDrawer
import org.arakhne.afc.nodefx.ZoomableGraphicsContext

import static extension org.arakhne.afc.simulation.framework.base.preferences.PreferenceManagerSingleton.*

/** Drawer for the fog.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
class FogDrawer extends AbstractMapCircleDrawer<FogZone> {

	/** Constructor.
	 */
	new {
	}

	def getFogColor(gc : ZoomableGraphicsContext) : Color {
		gc.rgb(typeof(FogPreferenceManager).singleton.fogColor)
	}

	def getMaximumVisibilityDistance : double {
		typeof(FogPreferenceManager).singleton.fogMaximumVisibilityDistance
	}

	def getOpacityFactor : double {
		typeof(FogPreferenceManager).singleton.fogOpacityFactor
	}

	
	def getPrimitiveType : Class<? extends FogZone> {
		typeof(FogZone)
	}

	def draw(gc : ZoomableGraphicsContext, element : FogZone) {
		gc.fill = gc.rgba(gc.getFogColor, element.visibility.toTransparency)
		val radius = element.radius
		val diameter = radius + radius
		val minx = element.x - radius
		val miny = element.y - radius
		gc.fillOval(minx, miny, diameter, diameter);
	}

	def toTransparency(visibility : double) : double {
		val dist = maximumVisibilityDistance
		getOpacityFactor * Math::max(0.0, dist - visibility) / dist
	}

}
