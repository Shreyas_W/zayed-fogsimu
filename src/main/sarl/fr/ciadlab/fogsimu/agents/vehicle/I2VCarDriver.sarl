/* 
 * $Id$
 * 
 * Copyright (c) 2019-2020 Stephane Galland and Zayed University.
 * 
 * This source code is not open-source. It is the joint proprietary of Stephane Galland
 * and the Zayed University, United Arab Emirates, according to the terms of the
 * contract passed between the parties.
 */
package fr.ciadlab.fogsimu.agents.vehicle

import fr.ciadlab.fogsimu.behaviors.Driver
import fr.ciadlab.fogsimu.events.I2VFogConditionDetected
import fr.ciadlab.fogsimu.skills.FoggyWeatherObstacleDetector
import fr.ciadlab.fogsimu.skills.I2VPanelRoadCruiseSpeedManager
import io.sarl.core.Behaviors
import io.sarl.core.Initialize
import io.sarl.core.Logging
import io.sarl.lang.core.Behavior
import java.util.Collection
import org.arakhne.afc.math.geometry.d2.d.Point2d
import org.arakhne.afc.simulation.framework.base.preferences.PreferenceManager
import org.arakhne.afc.simulation.framework.framework1d.agents.AbstractSimulatedAgent1d
import org.arakhne.afc.simulation.framework.framework1d.agents.cruisespeed.CruiseSpeedManager
import org.arakhne.afc.simulation.framework.framework1d.agents.destination.StandardDestinationTester
import org.arakhne.afc.simulation.framework.framework1d.preferences.PreferenceManager1d

import static extension org.arakhne.afc.simulation.framework.base.preferences.PreferenceManagerSingleton.*

/**
 * The agent that supports the driver behavior with the control of a car with I2V communication capabilities.
 * 
 * @author $Author: sgalland$
 * @version $FullVersion$
 * @mavengroupid $GroupId$
 * @mavenartifactid $ArtifactId$
 */
agent I2VCarDriver extends AbstractSimulatedAgent1d {

	uses Behaviors, Logging

	var driverBehavior : Behavior

	override overridableInitializationStage(it : Initialize, differedCode : Collection<Runnable>) : void {
		super.overridableInitializationStage(it, differedCode)
		loggingName = "DRIVER-" + ID.toString

		setSkill(new StandardDestinationTester(
			it.parameters.get(4) as Double,
			new Point2d(it.parameters.get(2) as Double, it.parameters.get(3) as Double)))

		val carFollowingModel = typeof(PreferenceManager1d).singleton.carFollowingModel
		val carFollowingSkill = carFollowingModel.skillType.newInstance
		setSkill(carFollowingSkill)

		val humanEstimationModel = typeof(PreferenceManager).singleton.humanEstimationModel
		val humanEstimationSkill = humanEstimationModel.skillType.newInstance
		setSkill(humanEstimationSkill)

		setSkill(new FoggyWeatherObstacleDetector)

		setSkill(new I2VPanelRoadCruiseSpeedManager)

		this.driverBehavior = new Driver(this)
		this.driverBehavior.registerBehavior(it.parameters)
	}

	on I2VFogConditionDetected {
		var sk = typeof(CruiseSpeedManager).getSkill
		if(sk instanceof I2VPanelRoadCruiseSpeedManager) {
			sk.fogDectionSignalReceived(occurrence)
		}
	}

}
