\chapter{Simulation Scenarios}\label{section:experimentscenarios}

\begin{graphicspathcontext}{{./img/experiments/},\old}

In this chapter, several experiments have been described in order to validate through experimentations the general behavior of the proposed simulator.

In order to understand the behavior of drivers in foggy weather conditions, several situations may be considered:
\begin{enumerate}
	\item[No smart system] The drivers have not dynamic information related to the fog situation.
	\item[Smart devices] The drivers perceives the road signs that are able to adapt their displays in order to reflect the foggy weather situation.
	\item[I2V communication] The drivers are notified on-board by the infrastructure about the foggy weather situation.
	\item[V2V communication] A peer-to-peer (local) communication is set up between the vehicles in order to propagate the weather information.
\end{enumerate}

These different cases are subject of different scenarios that are detailed below.

\section{Environment Description}

Whatever the simulation scenario, the environment definitions must shared the same base definition in order to enable the comparison of the results.

\subsection{General Description}

The selected environment is:

\begin{emphbox}
	A two-lane highway of 10 km long, without exit, entry or interchange ramps.
\end{emphbox}

The selected area is a portion of the E11 highway that is located in United Arab Emirats, as illustrated by Figure \figref{roadsmap}.

\mfigure{width=.95\linewidth}
{roadsmap}
{OSM representation (top) of a portion of the E11 highway (United Arab Emirats). The bottom part is the ESRI shape representation of this highway, used for simulation experiments.}
{roadsmap}

The vehicles will be injected into the simulation at the red mark, on the west part of the highway (vehicles follows the west-to-east direction).

Fog will be created at the yellow mark's position.
As described in Chapter \ref{section:simulationscenationdescription}, a foggy area is represented by a circle.
Therefore, the fog circle will be centered on the yellow mark and the radius of the fog area will be of \ParamFogRadius meters.

These values (position and radius) are parameters of the simulator.
They could be changed into the scenario description file, as described in Chapter \ref{section:simulationscenationdescription}.

Additionally, the density of the fog is represented by the visibility distance inside the fog area.
In the context of these experiments, the visibility is set to \ParamFogVisibility meters up.
Again, this value could be changed into the scenario description files.

\subsection{Input Parameters}

\begin{mtable}[h!]{\linewidth}{4}{|X|l|X|X|}{Input Parameters for the Environment Description}{datainputs:environment}
	\tabularheader{Description}{Source}{Format}{Default Value}
	Map of the roads & Resource \#1 & ESRI Shape file & \texttt{map-line.shp} \\
	\hline
	Definition of the legal speed limit per road type & General conf & km/h & \{ $120$, $110$, $80$, $70$, $50$, $30$, $20$ \} \\
	\hline
	Definition of the locations where vehicles are spawned into the system & Scenario file & $(x,y)$ & Red mark \\
	\hline
	Definition of the generation rate of the vehicles on the road & Scenario file & vehicles/hour & \ParamGenerationRate \\
	\hline
	Maximum number of vehicles that are generated during the scenario execution & Scenario file & vehicles & \ParamGenerationBudget \\
	\hline
	Definition of the target location of the vehicles at which they decide to quit the simulation & Scenario file & $(x,y)$ & Green mark \\
	\hline
	Definition of the location of the fog area & Scenario file & $(x,y)$ & Yellow mark \\
	\hline
	Definition of the radius of the fog area & Scenario file & meters & \ParamFogRadius \\
	\hline
	Definition of the fog density by internal visibility & Scenario file & meters & \ParamFogVisibility \\
	\hline
\end{mtable}

Table \tabref{datainputs:environment} provides the list of data inputs that are necessary to set up the environment description for simulation.

The map of the roads is basically a ESRI Shape file, which is a standard in Geographical Information Systems (GIS).
Nevertheless \href{http://openstreetmap.org}{Open Street Map} (OSM) is another standard in GIS.
It enables the access to a large amount of maps worldwide.
OSM could be freely downloadable from the OSM website.
Then, it is easy to convert the OSM file into an ESRI shape file, with an online service such as \href{https://mygeodata.cloud/}{MyGeoData Cloud}.

\section{Generation of Vehicles}

\subsection{Vehicle Generation Law}

When the simulation is running, vehicles must be created dynamically into the system according to a random rate (vehicles/hour).
The law for computing the number $nbVehicles$ of vehicles per hour that should be generated follows a uniform law (see Section \ref{section:nonuniformrandom:uniform} page \pageref{section:nonuniformrandom:uniform}), with the following parameters:
\begin{equation}
nbVehicles = \ParamGenerationRate \text{ veh/h}
\end{equation}

The budget of vehicles is of \ParamGenerationBudget vehicles for the whole scenario execution.

\subsection{Initial Lane Selection}

When it is created into the simulation, a vehicle must be assigned to a line of the initial road segment.
The number of lanes for a road is $L$.
The law for selected the initial lane at the position $laneIndex$ ($0$ is the first) is a linear law (see Section \ref{section:stochasticlaw:linear} page \pageref{section:stochasticlaw:linear}), with the following parameters:
\begin{equation}
\begin{aligned}
\text{laneIndex} : [0; L) \rightarrow & [s_m; s_x] \\
(i) \mapsto & \exists x | x \in [\xmin; \xmax]
\end{aligned}
\end{equation}

Such that:
\begin{equation}
\begin{cases}
\xmin = 0 \\
\xmax = L - 1 \\
ascendent = \text{false} \\
\delta = \xmin - \xmax = -1
\end{cases}
\end{equation}


\subsection{Initial Speed}

When it is created into the simulation, a vehicle must have an initial speed.
The scenario file permits to specify the range of speeds, in which the initial value will be chosen.
This range is represented by $[s_m; s_x]$, where $s_m$ is the minimum speed value, and $s_x$ the maximum speed value.
When a road has multiple lanes, it is usual that the average vehicle's speed on a lane is lower than the average speed on the left (right in UK-like road system standard) lanes.
The number of lanes for a road is $L$.

The law for selected the initial speed within the given range when the vehicle is located on lane $i$ is defined as:
\begin{equation}
\begin{aligned}
	\text{initialSpeed} : [0; L) \rightarrow & [s_m; s_x] \\
	(i) \mapsto & \exists s | s \in [\alpha; \beta]
\end{aligned}
\end{equation}

With the law parameters defined as:
\begin{equation}
\begin{cases}
\alpha = \max\left(s_m, i r - \dfrac{r}{2}\right) \\
\beta = \min\left(s_x, (i + 1) r + \dfrac{r}{2}\right) \\
r = \dfrac{s_x - s_m}{L}
\end{cases}
\end{equation}

Whatever the initial speed that is assigned to a vehicle, the initial acceleration is always equal to $0$ $m/s^2$.

\section{Output Indicators}

For all the scenarios, the simulator should provide the output indicates that are listed in Table \tabref{dataoutputs:scenario1}.

\begin{mtable}{\linewidth}{3}{|l|X|X|}{Output indicators for scenario 1}{dataoutputs:scenario1}
	\tabularheader{Symbol}{Name}{How it is measured}
	$d_\star$(t) & Density of vehicles & It is the average number of vehicles per kilometer during the simulation time. \\
	\hline
	$t_a$(t) & Travel time & It is the difference between the time of arrival at the target point and the time of arrival at the departure point. It is average for all the vehicles. \\
	\hline
	$i_a$(t) & Individual inter-vehicle distance & It is the average of the inter-vehicle distances for a single vehicle. \\
	\hline
	$i_\star$(t) & General inter-vehicle distance & It is the average of the inter-vehicle distances for all vehicles. \\
	\hline
	$fi_a$(t) & Individual inter-vehicle distance in the fog area & It is the average of the inter-vehicle distances for a single vehicle when it is inside the foggy weather zone. \\
	\hline
	$fi_\star$(t) & General inter-vehicle distance in the fog area & It is the average of the inter-vehicle distances for all vehicles when they are inside the foggy weather zone. \\
	\hline
	$\dot{v}_a$(t) & Individual speed & It is the average of the speed for a single vehicle. \\
	\hline
	$\dot{v}_\star$(t) & General speed & It is the average of the speeds for all vehicles. \\
	\hline
	$\dot{w}_a$(t) & Individual speed in fog area & It is the average of the speed for a single vehicle when it is inside the foggy weather zone. \\
	\hline
	$\dot{w}_\star$(t) & General speed in the fog area & It is the average of the speeds for all vehicles when they are inside the foggy weather zone. \\
	\hline
	$\overrightarrow{a}$(t) & Cumulative number of front accidents & It is the cumulative number of vehicles' accidents for the vehicles that have touched at the front of the car. \\
	\hline
	$\overrightarrow{fa}$(t) & Cumulative number of front accidents in fog & It is the cumulative number of vehicles' accidents for the vehicles that have touched at the front of the car into the fog area. \\
	\hline
	$\overleftarrow{a}$(t) & Cumulative number of back accidents & It is the cumulative number of vehicles' accidents for the vehicles that have touched at the back of the car. \\
	\hline
	$\overleftarrow{fa}$(t) & Cumulative number of back accidents in fog & It is the cumulative number of vehicles' accidents for the vehicles that have touched at the back of the car into the fog area. \\
	\hline
\end{mtable}

\clearpage

\section{Scenario 1: Highway without communication}

In this scenario, the drivers have not dynamic information related to the fog situation.
This scenario serves as the reference in the analysis and discussions related to the results (see Section \ref{section:simulationresults} page \pageref{section:simulationresults}).
The complete definition of the scenario 1's file is provided in Annex \ref{section:configurationfile:scenario1} page \pageref{section:configurationfile:scenario1}.

\mfigure{width=\linewidth}{commScenario1}{Scenario 1  includes neither communication between the infrastructure and the vehicles, nor between the vehicles}{comscenario1}

Figure \figref{comscenario1} shows a simplified view of the scenario 1.
Speed-limit panels are regular panels, i.e. they are neither smart nor connected.
The car drivers perceive the objects in the front of their cars with a maximum distance of \ParamPerceptionDistance meters by default (this distance could be changed in the general configuration of the simulator).

\subsection{Infrastructure Definition}

\begin{upmcaution}
	There is no special infrastructure to consider in this scenario.
	Indeed, all the agents knows the legal speed limit on the highway, that is specified in Table \tabref{datainputs:environment}.
\end{upmcaution}

\subsection{Driver Behavior}\label{section:driverbehavior:scenario1}

The general driver behavior is defined in Section \ref{section:driverbehaviors} page \pageref{section:driverbehaviors}.

This general behavior is adapted to this scenario on the following points:
\begin{enumerate}
	\item The driver has the goal to traverse the area $\Rightarrow$ the target point is the green mark on Figure \figref{roadsmap}.
	\item When the vehicle is reaching the target point, it disappears from simulation.
	\item The preferred cruise speed selected by the driver is the one at which the vehicle is created into the system. It is close to the legal speed limit, more or less an approximation factor.
	\item The safety distance that is assumed by the driver to the front vehicle is randomly selected in the ranges in Table \tabref{safetydistances}.
	\item \emph{The driver does not try to change lane.}
\end{enumerate}

\begin{mtable}{.7\linewidth}{2}{|X|X|}{Safety distances according to the vehicle speed, $\alpha = 1$}{safetydistances}
	\tabularheader{Vehicle Speed (km/h)}{Safety Distance (m)}
	$[0; 30)$ & $[1; 50]$ \\
	\hline
	$[30; 50)$ & $[50; 85]$ \\
	\hline
	$[50; 90)$ & $[85; 150]$ \\
	\hline
	$[90; 110)$ & $[150; 185]$ \\
	\hline
	$[110; 130)$ & $[185; 215]$ \\
	\hline
	$[130; +\infty)$ & $[215; 250]$ \\
	\hline
\end{mtable}

Inter-vehicle distance is the distance between the current vehicle and another vehicle in front of.
The safety distance is the minimal inter-vehicle distance under which it is assumed to be hard to avoid collision with the front car.
The safety distance $sd$ is evaluated by Equation \ref{eq:safetydistance}, where $\gamma_r$ is the reaction duration in seconds of the driver (usually $1$ second), and $\gamma_s$ is the expected time for stopping the vehicle in case of emergency (usually $5$ seconds).
$\dot{v}$ is the current speed of the vehicule (km/h).
$\alpha$ is an approximation factor that is usually a constant.

\begin{equation}\label{eq:safetydistance}
	sd = \alpha \left(\gamma_r + \gamma_s \right) \dfrac{1000 \dot{v}}{3600}
\end{equation}

Table \tabref{safetydistances} shows up the application of Equation \ref{eq:safetydistance} on standard speed ranges, where $\alpha = 1$.

In the general driver behavior's architecture, the following modules do nothing in the context of this scenario:
\begin{description}
	\item[Path selection] the path to follow is hard coded into the scenario.
	\item[Lane change] this behavior is assumed to be not existing in this scenario.
	\item[Communication receiver] the car is not connected.
	\item[Information emitter] the car is not connected.
\end{description}

The following modules of the driver's behaviors are specifically adapted to this scenario:
\begin{description}
	\item[Road sign management] the legal speed limit is hard coded according to the input parameter in Table \tabref{datainputs:environment}.
\end{description}


\subsection{Input Parameters}

This scenario needs input parameters for the driver agents.
The list is provided by Table \tabref{datainputs:scenario1and2}.
All these parameters must be provided within the general configuration of the simulator.

\begin{mtable}{\linewidth}{4}{|X|l|l|l|}{Input parameters for the scenarios 1 and 2}{datainputs:scenario1and2}
	\tabularheader{Description}{Source}{Format}{Default Value}
	Approximation factor for safety distance $\alpha$ & General conf & $\R+$ & $1$ \\
	\hline
	Reaction time of the drivers $\gamma_r$ & General conf & $\R+$ (seconds) & $1$ \\
	\hline
	Expecting duration of an emergency stop $\gamma_s$ & General conf & $\R+$ (seconds) & $5$ \\
	\hline
	Speed of the vehicle at the entry point on the road & General conf & $\R+$ (km/h) & random value in $[90;130]$ \\
	\hline
	Lane of the vehicle at the entry point on the road & General conf & $\text{P}(on right)$ (probabiliy) & $0.75$ \\
	\hline
\end{mtable}


\section{Scenario 2: Highway with smart infrastructure and I2I communication}

In this scenario, the drivers are informed by road signs about a foggy weather situation.
This information is not done by communication, but by visual perception of the driver.
The complete definition of the scenario 2's file is provided in Annex \ref{section:configurationfile:scenario2} page \pageref{section:configurationfile:scenario2}.

\mfigure{width=\linewidth}{commScenario2}{Scenario 2 includes I2I communication between the infrastructure components}{comscenario2}

Figure \figref{comscenario2} shows a simplified view of the scenario 2.
Speed-limit panels are smart and connected to the other speed-limit panels.
The maximum distance of communication between the smart panels is $2,500$ meters.


\subsection{Infrastructure Definition}\label{section:infrastructuredefinitionscenario2}

Road sign panels are put every 2 kilometers along the road.
They are able to adapt their display by showing the best speed limit in the current weather conditions.

\begin{lstlisting}[caption={Algorithm of the smart speed limit panels}, label={smartspeedlimitpanelbehavior}, frame=lines, xleftmargin=0.7cm, xrightmargin=0.5cm]
def smartPanelBehavior {
	if (panel.inFog) {
		panel.display(speedLimitInFog)
		emitI2Xsignals(panel.position,
		               speedLimitInFog,
		               speedLimitInFog)
	} else {
		if (I2IFogConditionDetected (*@$\neq \emptyset$@*)) {
			var ds = distance(
			       panel.position,
			       I2IFogConditionDetected.signalSourcePosition)
			if (ds (*@$\le\theta_i$@*)) {
				var sif = I2IFogConditionDetected.speedLimit
				var fp = I2IFogConditionDetected.fogPosition
				var df = distance(
				         panel.position,
				         fp)
				var (*@$\alpha$@*) = df / (*@$\theta_i$@*)
				var (*@$\alpha_c$@*) = min(1, (*@$\alpha$@*))
				var (*@$\beta$@*) = (*@$|$@*)legalSpeedLimit - sif(*@$|$@*) * (*@$\alpha_c$@*)
				var s = min(legalSpeedLimit, sif) + (*@$\beta$@*)
				s = s.normalize
				panel.display(s)
				if (s < legalSpeedLimit) {
					emitI2Xsignals(fp, sif, s)
				}
			} else {
				panel.display(legalSpeedLimit)
			}
		} else {
			panel.display(legalSpeedLimit)
		}
	}
}
def emitI2Xsignals(fogPos : Point2d, sif : double,
                   sp : double) {
	emit(new I2IFogConditionDetected => {
		fogPosition = fogPos;
		signalSourcePosition = panel.position;
		speedLimit = sif
	})
}
\end{lstlisting}

The behavior of the smart panels is defined in Listing \ref{smartspeedlimitpanelbehavior}.
The function \code{smartPanelBehavior} defines the general behavior of the smart panel.
The function \code{emitI2Xsignals} explicitly emits the I2X signals.

When the panel detects fog, i.e. it is located into a foggy weather zone, it assumes that the maximum speed for a vehicle is the result of Equation \ref{eq:speedlimitinfog}.
At the same time, the panel broadcasts a communication event to the neighbor panels with its position.

Equation \ref{eq:speedlimitinfog} represents the maximal speed in foggy area in order to avoid collision.
It is based on the visiblity distance in the fog and the stopping distance that is the sum of the reaction distance (Equation \ref{eq:reactiondistance}) and the braking distance\footnote{Source: \url{https://korkortonline.se/en/theory/reaction-braking-stopping/}} (Equation \ref{eq:brakingdistance}).

The reaction distance $d_{react}$ is the distance the vehicle travels from the point of detecting a hazard until it begins braking or swerving:
\begin{equation}\label{eq:reactiondistance}
d_{react} = \dfrac{v \times \gamma_r}{3.6}
\end{equation}
Where, $v$ is the speed in km/h; $\gamma_r$ is the reaction duration in seconds of the driver.

The braking distance $d_{brake}$ is the distance the vehicle travels from the point when you start braking until the car stands still:
\begin{equation}\label{eq:brakingdistance}
d_{brake} = \dfrac{v^2}{250 \times \mu}
\end{equation}
Where, $\mu$ is the coefficient of friction, approx. $0.8$ on dry asphalt and $0.1$ on ice.
Consequently, the stopping distance is given by:
\begin{equation}
d_{stop} = d_{react} + d_{brake}
\end{equation}

In order to estimate the best speed to stop safely under foggy weather condition, the following equation should be solved, in which the previous $v$ is renamed by $speedLimitInFog$ and $visibilityInFog$ is the distance of visibility into the fog:
\begin{equation}
visibityInFog = d_{stop} = \dfrac{speedLimitInFog \times \gamma_r}{3.6} + \dfrac{speedLimitInFog^2}{250 \times \mu}
\end{equation}

Resolution of the previous equation is, with $f = visibilityInFog$ and $x = speedLimitInFog$:

\begin{align}
f =& \dfrac{250 \gamma_r \mu x + 3.6 x^2}{900 \mu} \\
900 \mu f =& 3.6 x^2 + 250 \gamma_r \mu x \\
3.6 x^2 + 250 \gamma_r \mu x - 900 \mu f =& 0
\end{align}

\begin{equation}
\begin{cases}
a = 3.6 \\
b = 250 \gamma_r \mu \\
c = - 3.6 \times 250 \mu f
\end{cases}
\end{equation}

\begin{align}
\Delta = b^2 - 4 a c =& \left(250 \gamma_r \mu\right)^2 - \left(4 \times 3.6 \times -3.6 \times 250 \mu f\right) \\
=& 62500 \gamma_r^2 \mu^2 + 12960 \mu f \\
\Delta >& 0
\end{align}

\begin{equation}
\begin{cases}
x_1 = \dfrac{-b - \sqrt{\Delta}}{2a} = \dfrac{-250 \gamma_r \mu - \sqrt{\Delta}}{7.2}\\
x_2 = \dfrac{-b + \sqrt{\Delta}}{2a} = \dfrac{-250 \gamma_r \mu + \sqrt{\Delta}}{7.2} \\
x_1 \le x_2
\end{cases}
\end{equation}

The ``positive'' root from the equation resolution is selected:
\begin{equation}\label{eq:speedlimitinfog}
speedLimitInFog = \dfrac{\sqrt{62500 \gamma_r^2 \mu^2 + 12960 \mu \times visibilityInFog} - 250 \gamma_r \mu}{7.2}
\end{equation}

When the smart speed limit panel is not in the fog zone, it may have two different behaviors.
First, if it has not received an event related the foggy weather condition or the event is related to a too far fog zone (distance greater than $\theta_i$ meters), it displays the legal speed limit.

If such an event was received within a distance lower than or equal to $\theta_i$ meters, it displays an interpolated speed between the $speedLimitInFog$ and the legal speed limit according to the distance to the initial fog source.

The function \code{normalize} applies a normalization rule that approximate to the closest lower well-know speed, such as $\{0, 30, 50, 70, 90, 110, 130\}$ (in France).

\subsection{Driver Behavior}

\begin{upminfo}
	The behavior of the driver in Scenario 2 is exactly the same as the driver behavior in Scenario 1 (see Section \ref{section:driverbehavior:scenario1} page \pageref{section:driverbehavior:scenario1}).
\end{upminfo}


\subsection{Input Parameters}

This scenario needs input parameters for the agents that are in charge of the smart panels.
The list is provided by Table \tabref{datainputs:scenario1and2} page \tabpageref{datainputs:scenario1and2} and in Table \tabref{datainputs:scenario2only}.
All these parameters must be provided within the scenario file or the general configuration of the simulator.

\begin{mtable}{\linewidth}{4}{|X|l|l|l|}{Additional input parameters for the Scenario 2}{datainputs:scenario2only}
	\tabularheader{Description}{Source}{Format}{Default Value}
	Distance $\theta_i$ of propagation of the fog situation events & General conf & $\R+$ meters & $2,500$ \\
	\hline
	Duration $\Theta_i$ of the routing of the fog situation events. This value permits to control the delay of propagation of the events between the smart panels & General conf & $\R+$ seconds & $1$ \\
	\hline
\end{mtable}


\section{Scenario 3: Highway with I2X communication}

In Scenario 3, the previous scenario is extended with I2V communication: the vehicles are notified on-board by the infrastructure about the foggy weather situation (Infrastructure to Vehicle communication).
The complete definition of the scenario 3's file is provided in Annex \ref{section:configurationfile:scenario2} page \pageref{section:configurationfile:scenario3}.

\mfigure{width=\linewidth}{commScenario3}{Scenario 3 includes I2V communication between the infrastructure and the vehicles in addition to the I2I communication}{comscenario3}

Figure \figref{comscenario3} shows a simplified view of the scenario 3.
Speed-limit panels emit signals to the vehicles in addition to the signals that are emitted to the other smart panels.
The maximum distance of communication from a smart panel to a vehicle is $1,000$ meters.


\subsection{Infrastructure Definition}
\label{section:infrastructuredefinitionscenario3}

THe infrastructure definition in Section \ref{section:infrastructuredefinitionscenario2} page \pageref{section:infrastructuredefinitionscenario2}) is extended in order to emit specific I2V signals to the connected vehicles.
Listing \ref{i2vsmartspeedlimitpanelbehavior} redefines the function \code{emitI2Xsignals} in order to emit the I2I frog detection signals to the other smart panels, and the I2V frog detection signals to the connected vehicles.

\begin{lstlisting}[caption={Algorithm of the smart speed limit panels with both I2I and I2V communications}, label={i2vsmartspeedlimitpanelbehavior}, frame=lines, xleftmargin=0.7cm, xrightmargin=0.5cm]
def emitI2Xsignals(fogPos : Point2d, sif : double,
                   sp : double) {
	emit(new I2IFogConditionDetected => {
		fogPosition = fogPos;
		signalSourcePosition = panel.position;
		speedLimit = sif
	})
	
	every(1/(*@$\kappa$@*) seconds) [
		emit(new I2VFogConditionDetected => {
			fogPosition = fogPos;
			signalSourcePosition = panel.position;
			speedLimit = sif;
			speedLimitOnPanel = sp
		})
	]
}
\end{lstlisting}

The \code{I2VFogConditionDetected} signal is emitted at a given rate $\kappa$, which is the number of signals to be emitted per second.
The \code{I2VFogConditionDetected} signal contains the following data:
\begin{itemize}
	\item \code{fogPosition}: the position of the first smart panel that has detected the foggy weather condition;
	\item \code{signalSourcePosition}: the position of the panel that emits the signal;
	\item \code{speedLimit}: the speed limit that is computed in order to avoid collision into the fog;
	\item \code{speedLimitOnPanel}: the speed limit that is displayed onto the road sign that emits the signal.
\end{itemize}

\subsection{Driver Behavior}

The driver behavior of the scenarios 1 and 2 is extended.
In order to support the receiving of I2V communication events, only the ``Communication receiver'' module should be adapted.
In this scenario, this module extracts information about the foggy weather condition when it receives a \code{I2VFogConditionDetected} event.
This information is used for updating the desired/cruise speed of the vehicle.

\begin{lstlisting}[caption={Algorithm of the connected car with I2V communication}, label={driverbehaviorcode:scenario3}, frame=lines, xleftmargin=0.7cm, xrightmargin=0.5cm]
var foggySpeedLimit = (*@$+\infty$@*)

on I2VFogConditionDetected {
	var d = distance(car.position,
	                 occurrence.signalSourcePosition)
	if (d (*@$\le\theta_{iv}$@*)) {
		foggySpeedLimit = I2VFogConditionDetected
		                  .speedLimitOnPanel
	} else {
		foggySpeedLimit = (*@$+\infty$@*)
	}
}

def computeCruiseSpeed {
	min(foggySpeedLimit, legalSpeedLimit)
}
\end{lstlisting}

In Listing \ref{driverbehaviorcode:scenario3}, the driver agent determines the minimum speed limit that is provided through the I2V communication from the smart speed limit panels below a distance of $\theta_{iv}$ meters (i.e. the maximal I2V communication distance).
The speed limit that is notified to the driver is the one displayed on the smart speed limit panel that is the source of the I2V signal.

\subsection{Input Parameters}

This scenario needs input parameters for the agents that are in charge of the driver behavior.
The list is provided by Table \tabref{datainputs:scenario1and2} page \tabpageref{datainputs:scenario1and2}, Table \tabref{datainputs:scenario2only} and Table \tabref{datainputs:scenario3only}.
All these parameters must be provided within the scenario file or the general configuration of the simulator.

\begin{mtable}{\linewidth}{4}{|X|l|l|l|}{Additional input parameters for the Scenario 3}{datainputs:scenario3only}
	\tabularheader{Description}{Source}{Format}{Default Value}
	Distance $\theta_{iv}$ of propagation of the fog situation events in I2V communication & General conf & $\R+$ meters & $1,000$ \\
	\hline
	Duration $\Theta_{iv}$ of the routing of the fog situation events in I2V communication. This value permits to control the delay of propagation of the events between the smart panels and the vehicles & General conf & $\R+$ seconds & $2$ \\
	\hline
	Number of I2V signals that are emitting per second as soon as the foggy weather condition is detected & General conf & $\R+$ sig/s & $2$ \\
	\hline
\end{mtable}


\section{Scenario 4: Highway with V2V communication}

In this scenario, the drivers are notified through a local communication between the vehicles.
The complete definition of the scenario 4's file is provided in Annex \ref{section:configurationfile:scenario4} page \pageref{section:configurationfile:scenario4}.

\mfigure{width=\linewidth}{commScenario4}{Scenario 4 includes V2V communication between the vehicles in addition to the I2I and I2V communication}{comscenario4}

Figure \figref{comscenario4} shows a simplified view of the scenario 4.
Vehicles are equipped for signal emission devices that permit to exchange V2V signals between the vehicles.
The maximum distance of communication between two vehicles is $400$ meters.


\subsection{Infrastructure Definition}

\begin{upminfo}
	The infractructure definition is the same as in Scenario 3 (see Section \ref{section:infrastructuredefinitionscenario3} page \pageref{section:infrastructuredefinitionscenario3}).
\end{upminfo}


\subsection{Driver Behavior}

The driver behavior of the scenario 3 is extended.
In order to support the V2V communication, the ``Communication emitter'' module should be implemented.
In this scenario, this module emits \code{FogConditionDetected} events that contain all the information related to the known foggy weather condition.

\begin{lstlisting}[caption={Algorithm of the connected car with V2V communication}, label={driverbehaviorcode:scenario4}, frame=lines, xleftmargin=0.7cm, xrightmargin=0.5cm]
var foggySpeedLimitFromOtherVehicles = (*@$+\infty$@*)

on V2VFogConditionDetected {
	var d = distance(car.position,
				occurrence.signalSourcePosition)
	if (d (*@$\le\theta_{vv}$@*)) {
		foggySpeedLimitFromOtherVehicles =
		   V2VFogConditionDetected.speedLimitForAheadCar
	} else {
		foggySpeedLimitFromOtherVehicles = (*@$+\infty$@*)
	}
}

def computeCruiseSpeed : double {
	var speed = min(
		foggySpeedLimitFromOtherVehicles,
		foggySpeedLimit,
		legalSpeedLimit)
	if (foggySpeedLimitFromOtherVehicles (*@$\neq \emptyset$@*)) {
		emit(new V2VFogConditionDetected => {
			fogPosition = V2VFogConditionDetected.fogPosition;
			signalSourcePosition = car.position;
			speedLimit = V2VFogConditionDetected.speedLimit;
			speedLimitForAheadCar = speed
		})
	}
	return speed
}
\end{lstlisting}

In Listing \ref{driverbehaviorcode:scenario4}, the driver agent saves any information related to a detected foggy weather situation.
When such information is available, it emits a V2V communication event with the description of the information.

\subsection{Input Parameters}

This scenario needs input parameters for the communication module of the driver agents.
The list of input parameters inherited from the previous behaviors is provided in Table \tabref{datainputs:scenario1and2} page \tabpageref{datainputs:scenario1and2} and Table \tabref{datainputs:scenario2only} page \tabpageref{datainputs:scenario2only}.
Specific input parameters for Scenario 4 are provided in Table \tabref{datainputs:scenario4}.
All these parameters must be provided within the scenario file.

\begin{mtable}{\linewidth}{4}{|X|l|l|l|}{Additional input parameters for the Scenario 4}{datainputs:scenario4}
	\tabularheader{Description}{Source}{Format}{Default Value}
	Distance $\theta_{vv}$ of propagation of the fog situation events in V2V communication & General conf & $\R+$ meters & $300$ \\
	\hline
	Duration $\Theta_{vv}$ of the routing of the fog situation events in V2V communication. This value permits to control the delay of propagation of the events between the vehicles & General conf & $\R+$ seconds & $1.5$ \\
	\hline
\end{mtable}

\end{graphicspathcontext}
