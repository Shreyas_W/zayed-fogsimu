\chapter{General Simulator Architecture}\label{section:simulatorarchitecture}

\begin{graphicspathcontext}{{./img/architecture/},\old}

\section{The principles sustaining the architecture}

When defining a multi-agent simulation architecture\index{Simulation!Simulator Architecture}, it is required to specify how the agents will interact with their environment, and how this interaction will be performed (i.e. how it will impact the environment and the other agents). Several principles are stated in the literature for the definition of the architecture of a multi-agent based simulator. In order to define the architecture for a SARL simulator, we rely on these principles to ensure the simulation will be valid.

These principles are:
\begin{enumerate}[P1:]
    \item The separation between the physical representation of the agent in the environment (its body) and the agent decision process (its mind): to prevent an agent from directly applying actions on its body
    \item The influence-reaction model: to properly manage simultaneous actions
    \item The autonomy and the pro-activity of the agent: an agent is autonomous, it can make decisions and express influences at will without being constrained to reacting to a \textit{simulation tick}
    \item The agnosticism of the agent: an agent is not supposed to know how the simulation is managed, it should make decisions regardless of the implementation of the simulation
\end{enumerate}

\subsection{The body/mind Separation}\label{section:bodymind}

A first principle for the design of the SARL simulation framework is the separation between the agent domain (which contains the mental state of the agents and where the decisions are performed) and the environment domain (which contains the physical state of the agents) \citep{saunier2015agent}. This separation forbids an agent to directly change its physical state: the only way to change its physical state is through the environment interface.
This principe is illustrated by Figure \figref{body_mind}.

\mfigure{width=\linewidth}{agent_env_interface}{The body at the interface between the agent mind and the environment, extracted from \citep{galland2009environment}}{body_mind}

It can be noted that in this model, the body\index{SARL!Metamodel!Body} of an agent is its only interface to the environment. This means that it can only interact with the environment through the capacities of its body, and it can only perceive the environment through the sensors of its body. Thus, the environment will be in charge of the realization of the bodies capacities and in charge of providing perceptions to the bodies.

In the context of this project, the body/mind separation principles will cause a difference between the driver and the car:

\begin{upmcaution}
	\Emph{Simulated Vehicle = Driver + Car}\newline
	\Emph{Driver $\neq$ Car}\newline
	The concepts of driver and car are different. Both of them are representing different entities into the simulated system.
\end{upmcaution}

\begin{upminfo}
	\Emph{Driver:}\newline 
	It supports the driving behavior, i.e. the decision processes for controlling a vehicle.
	It is represented by the agent itself. The driver behavior is part of the agent behavior.
\end{upminfo}

\begin{upminfo}
	\Emph{Car:}\newline 
	It represents the physical part of the simulated vehicle, i.e. the physical properties of a vehicle (position, speed, acceleration, etc.). It is represented by the concept of mobile agent body, that is defined as an object of the environment.
\end{upminfo}


\subsection{The influence-reaction model}

The body/mind separation\index{SARL!Metamodel!Body} being stated, it allows us to set the influence/reaction principle\index{Influence-Reaction Model} defined in \citep{ferber1996influences} and refined in \citep{michel2007irm4s}. Following this principle, an agent is not directly performing \textit{actions} on the environment (which would be immediately applied): instead, it expresses \textit{intentions}. When conflicting intentions occurs (for instance, two agents want to go to the exact same place at the same time), the environment will resolve them and compute the corresponding reaction. This way the issues of simultaneous actions are solved (fig. \figref{simultaneous_actions}): with the Influence/Reaction model, the environment is managing the simultaneity of intentions and compute the resulting reaction, leading to the situation 3.

\mfigure{width=.7\linewidth}{simultaneous_actions}{Action versus Influence/Reaction, extracted from \citep{michel2006modele}}{simultaneous_actions}


Following the stated principles, it lead us to the model given in fig. \figref{environment_model} clearly stating the separation between the agent domain and the environment domain, with the interface between both being materialized by the agent's body.

\mfigure{width=\linewidth}{environment_model}{Environment model as stated in \citep{galland2009environment}}{environment_model}


\subsection{The autonomy and the agnosticism of the simulated agent}

The proposed framework should ensure the autonomy of the agents: they should be able to take their decisions autonomously without any constraint imposed by the simulation framework or the environment.

Thus, we define the agents as being \emph{simulation agnostic}, meaning they have no knowledge on how the simulation is run. This statement has several implications:

\begin{itemize}
    \item There is no \emph{simulation tick} triggering the decision process of the agents, an agent can make decisions and express influences at will, usually in reaction to its perceptions
    \item The perceptions are not expected to arrive simultaneously (they can, but the agent is not supposed to know if it is the case)
    \item They do not know if and when their influences will be applied, they cannot base their decisions on their internal time and an expectation on the delay of application of the influences; if required, the confirmation of the application of the influences should be provided by the perceptions
\end{itemize}

\section{The architecture of the simulator}

\subsection{Agent domain and environment domain}

\mfigure{width=\linewidth}{sim_arch}{Global architecture of the simulator: the \emph{Agent domain} and the \emph{Environment domain} linked by \emph{agent-environment interaction model, including influence-reaction model}}{sim_arch}

To apply this model using SARL, some details must be given to shed light on the way the agent domain and the environment domain are materialized, as illustrated by Figure \figref{sim_arch}, and how the communication between the two different domains is managed.

The agent domain is composed of agents representing the simulated entities, e.g. vehicles.
In order to benefit from the possibilities of the SARL language in terms of expression of the behaviors and the interactions, we decide to write everything related to the mind of the agents using the SARL language.

On the other hand, we have the environment domain. This environment will be in charge of all the management of the environment's objects, the analysis of the influences, and the computation of the perceptions for the agent bodies.
In order to be accessed from the agent domain, the environment domain must expose an interface allowing the simulation of the environment to be controlled, to emit influences, and to retrieve the perceptions.

We separate the required API in three parts:
\begin{itemize}
    \item the simulation control,
    \item the reception of the influences,
    \item the emission of the perceptions.
\end{itemize}

The simulation control part provides functions for:
\begin{itemize}
    \item Starting and pausing the simulation.
    \item Running the simulation for a given amount of time.
\end{itemize}

The parts for the reception of the influences and the emission of the perceptions are specific to the kind of environment we need to simulate. It is preferable to separate them: the emission of the perceptions should not be conditioned by the reception of the influences.
We will respectively refer to these methods as \code{retrievePerceptions : Collection<Perception>} and \code{emitInfluences(influences : Collection<Influence>)}.
The first one refers to the method exposed by the environment API to allow to get the perception of the agents.
While the second one refers to the methods exposed by the environment API to allow to apply influences.

\subsection{The junction between the SARL agents and the environment: the Environment Agent}

The SARL agents we want to simulate should not directly access to the API of the environment.
An intermediate agent, called \textit{Environment Agent}, is in charge of managing the access to the environment data. It has multiple roles:
\begin{itemize}
    \item Collect perceptions from the environment and transmit them to the agents.
    \item Collect influences from the agents and transmit them to the environment.
    \item Manage the execution of the simulation.
    \item Manage the attribution of the bodies to the agents.
\end{itemize}

The different roles of the environment agent are illustrated in Figure \figref{agent_organization} according to the capacity identification activity of the ASPECS methodology \citep{CossentinoGaudHilaireGallandKoukam2010_1}.

\mfigure{width=.83\linewidth}{Archi_Organizations}{Organizations in the simulator}{agent_organization}

The \texttt{Environment Model} contains the definition of the objects into the simulated environment, and their relationships. This part of the model is described into Section \ref{section:environmentmodel}.

The \texttt{Perception Computer} has the role to compute the set of objects in the fields of view of each agent. It iterates on the \texttt{Environment Model} for extracting the objects' descriptions, and providing them to the agent's \texttt{Body}.

The \texttt{Influence Analyzer} has the role to collect the influences that are provided by each agent's \texttt{Body}. Then, it runs an algorithm for detecting conflict among influences and solving them. The result of this analysis of the influences is a set of actions. Each each changes the state of the \texttt{Environment Model}.

The \texttt{Time Manager} has the role to store the current simulation time and provides functions for evolving this time.

The \texttt{Simulation Controller} has the role to schedule the simulation. Details are provided in Section \ref{section:simulationlifecycle}.

The organization \texttt{view} provides the support for external software components, such as Java FX graphical components, that ar able to show the state of the \texttt{Environment Model} up.

The organization \texttt{Agents} contains the simulated agents, i.e. the vehicles and the smart objects. The \texttt{Agent Body} is the same object as the \texttt{Body} in the \texttt{Environment} organization. The \texttt{Agent Mind} is the representation of an agent behavior that is controlling a body.


\end{graphicspathcontext}

