\chapter{Agent Domain}\label{section:agentdomain}

\begin{graphicspathcontext}{{./img/architecture/},\old}

The models for multiagent simulation are composed of four parts \citep{Michel.04.thesis, GaudGallandGechterHilaireKoukam2008_2}: \begin{inlineenumeration}
	\item \emph{Behaviors of the agents:} modeling of the deliberative processes of the agents,
	\item \emph{Interactions:} modeling the actions and the interactions among the agents,
	\item \emph{Environment:} specification and definition of the objects that make up the simulated world, as well as the endogenous dynamics of the environment,
	\item \emph{Scheduling and running:} modeling the passage of time, and definition of the agents' scheduling.
\end{inlineenumeration}
The boundary between what is considered as an agent and what is considered as part of the environment depends on the considered problem. However, agents always represent the active components of the studied problem.

A number of basic principles should be followed to ensure the quality of a simulation and limit through simulation: \begin{inlineenumeration}
	\item respect the integrity constraints of the environment,
	\item respect the constraint of locality agents,
	\item a clear distinction between the body of the agent (state variables beyond the control of the agent and part of the representation of the agent in the environment, \textit{e.g.} its speed, position\dots), and the mind of the agent (the state variables under the control of the agent, for example, emotionalism, its goals, motivations\dots)
\end{inlineenumeration}

\mfigure{width=.8\linewidth}{general_architecture}{General architecture of the simulator for a virtual environment}{general_simulator_architecture}

We propose a simulation model respecting these principles to create simulation models of individuals in virtual worlds. The architecture of our model is shown in the upper part of Figure~\figref{general_simulator_architecture}. Each agent can interact with other agents, either directly or by stigmergy. The agent can perceive and act on the physical environment through its representation in it: \emph{its body}. The lower part of the figure shows the display software. Note the presence of an ``avatar.'' The avatar is a representation of a real human user in the simulated universe. To ensure the consistency of the interactions between the avatar and the agents populating the universe, the avatar is subject to the same constraints as the bodies of the agents (imposed by the principles above): the avatar respects the laws of the virtual world, and cannot act outside their boundaries.

\begin{upmcaution}
	\Emph{Driver $\neq$ Car}\newline The concepts of driver and car are different. Both of them are representing different entities into the simulated system.
\end{upmcaution}

\begin{upminfo}
	\Emph{Driver:}\newline 
	It is represented by the agent itself. The driver behavior is part of the agent behavior.
\end{upminfo}

\begin{upminfo}
	\Emph{Car:}\newline 
	It is represented by the concept of mobile agent body, that is defined as an object of the environment.
\end{upminfo}

This chapter is specifically dedicated to the description of the models that are inside the agents.
These models will represent the behaviors of the drivers in the different situations within the context of this project.

\section{Driver Behaviors}\label{section:driverbehaviors}

This section describes the models of behaviors for the simulated drivers: standard drivers and connected drivers.
A specific section is dedicated to the adaptation of the model in order to consider foggy weather conditions into the agent decisions.

\subsection{Standard Driver}

The architecture used to model agents is composed of three layers, as shown in Figure~\figref{agent_layer}:
\begin{itemize}
	\item \textbf{Strategy Layer:} At the strategic level, individuals decide on the activities to be carried out in the universe.
	While some of these activities may be discretionary (\textit{e.g.} buying a newspaper), others may be mandatory (validating a ticket before entering the train). All choices can be linked to environmental characteristics (type and location of stores\dots) In this layer, agents typically use an architecture for selecting actions such as the Belief-Desire-Intention (BDI) architecture or the Goal-Oriented Action Planning model \citep{Orkin03}.
	\item \textbf{Tactic Layer:} Decisions at the tactical level concern the short or medium term. They must be taken by the agent with the decisions at the strategic level as reference. 
	From the objectives given by the latter, the model of the tactic layer must build a detailed action plan.
	The locations of the different activities and the paths to reach these locations are determined at this level. The BDI architecture can be used to construct the sequence of actions. Shortest path searching algorithms can form the basis for calculation of routes: A* \citep{Dechter:1985:GBS:3828.3830}, D* \citep{Stentz.icra94}.
	\item \textbf{Operation Layer:} At the operational level, individuals take decisions in the short term. 
	These decisions are guided by those provided by the previous layer.
	The features within this layer decide the trajectory of an individual, the speed or acceleration. 
	Unlike~\citet{Hoogendoorn.01}, who consider only the choice of the best speed for an individual, we believe that the main objective of the individual's model is to avoid collisions with the objects and the other individuals in the surrounding environment. 
	In other words, we consider that the models in the operational layer aim to determine the movements and actions that will make the body of an individual.
\end{itemize}

\mfigure{width=.5\linewidth}{agent_layers}{Layered architecture of an agent}{agent_layer}

There are interactions between the models for each decision level. Figure~\figref{agent_layer} provides an example of interactions between the layers constituting a simulation model of the driver.
This architecture is also used for the simulation of pedestrians and cyclists in virtual cities \citep{CrowdSimu.Thalmann.2007, Paris.07, BuissonGallandGaudGoncalvesKoukam2013_559}, in railway stations and airports \citep{Daamen.04, Demange2012}, to simulate the evacuation of a building in case of fire \citep{Geramifard.05}.

\begin{upminfo}
The strategic layer of the agent's architecture is outside the scope of this project. Only the tactic (Section \ref{section:pathcomputation}) and operation (Section \ref{section:drivingacceleration}) layers are detailed.
\end{upminfo}

\subsubsection{Path Computation}\label{section:pathcomputation}

One of the basic problems of network modeling is to find the shortest path from an origin to a destination.
In \citeyear{Dantzig1957}, \citeauthor{Dantzig1957} presented a conference paper that included the first formulation of the shortest path problem.
His paper was subsequently published in Operations Research \citep{Dantzig1957}.
Based upon that paper, \citet{Minty1957} suggested a format for solving the shortest path problem using a network represented as a web of strings and knots, and \citet{Ford1956} developed an algorithm to solve the shortest path problem in the presence of some negative arc lengths.
\citet{Dijkstra1959} followed the work of \citeauthor{Ford1956} with a new algorithm that appeared to be considerably more efficient and required less data storage.
The algorithm of \citeauthor{Dijkstra1959} remains to this day one of the best approaches for optimally solving the simple shortest path problem where all arcs have non-negative lengths.

In another early contribution, \citet{Hart1968} developed a search strategy, called A*, to solve for minimum cost paths.
The A* approach differs from other methods as it incorporates an estimate of the cost of ``path-completion''.
For certain classes of estimating functions, A* will find the optimal path.

Over the past 50 years, there has been continued and sustained interest in developing faster algorithms for solving the shortest path problem, because this problem is applied in a wide variety of areas including telephone call routing on communication networks and vehicle routing on road networks.

Over the last decade, there have been several notable attempts to compare shortest path algorithms \citep{Cherkassky1996, Zhan1998}.
Notably lacking from past work is a head-to-head comparison of A* and efficient implementations of \citeauthor{Dijkstra1959}.
This is somewhat understandable, given that A* requires a function for estimating the completion cost of a path.
Because of this, A* is not suitable for many shortest path applications; however, it is potentially ideal for an application using information sourced from GIS.

One of the earliest tests of A* is due to \citet{Golden1978} on Euclidean networks, where they found that A* expanded no more than about 10\% of the nodes than what would be expanded by the \citeauthor{Dijkstra1959} algorithm.
This possibility was also recognized by \citet{Shekhar1993, Shekhar1996} where they conducted preliminary tests of A* applied to the street network of Minneapolis and compared it to the \citeauthor{Dijkstra1959} algorithm as well as a ``breadth first'' search method.
While developing an approach to solve for shortest paths on German roads, \citet{Ertl1998} essentially dismissed A* as a viable alternative when he stated that A*, at best, is modestly faster than the \citeauthor{Dijkstra1959} algorithm.
Thus, virtually all previous research has either ignored the existence of A*, tested A* on a specialized problem or concluded that it is perhaps as good as or modestly better than the \citeauthor{Dijkstra1959} algorithm.

In a recent review, \citet{Fu2006} stated that A* is competitive to \citeauthor{Dijkstra1959}-based approaches, but then emphasized the possible advantages of using A* as a heuristic instead of as an algorithm.
Thus, it is impossible to discern from past work whether A* is a competitive technique to the most efficient encoding of the \citeauthor{Dijkstra1959} algorithm or a ``label-correcting'' algorithm like the two queues method of \citep{Pallottino1984}.

Recently, \citet{Zeng2009} demonstrate the relative value of A* in solving simple origin-to-destination shortest path problems on real road networks with respect to what is considered to be state-of-the-art encodings of \citeauthor{Dijkstra1959}’s and label-correcting algorithms.
The results, derived from real road networks, are extremely valuable for application developers and researchers in the GIS community.
\emph{This leads us to selected A* as the default approach that is used by the driver agents for computing their path on a road network.}

\subsubsection{Acceleration Computation with a Car Following Model}\label{section:drivingacceleration}

In order to simulate drivers, the agents should decide the instant acceleration of the vehicle at any time $t$.
To compute this acceleration, several models were proposed in literature. They are named ``car following models''.
In this project, three models are implemented:
\begin{enumerate}
	\item Reaction Time-based Collaborative Velocity Control --- RT-CVC \citep{Hao2017};
	\item Intelligent Driver Model --- IDM \citep{PhysRevE.62.1805};
	\item Intelligent Driver Model Plus --- IDM+ \citep{Schakel10}.
\end{enumerate}

Whatever the type of car following model, the maximum speed on a portion of the road should be determined.
We could consider that this maximum speed is mainly based on the driver's reading of the traffic signs.
However, there is cases where the agent should drive bellow the speed limit, e.g., turning at a crossroad, following a curved road. Consequently, the \emph{maximum comfortable speed} under these conditions should be computed.
This speed is based on the $V_{85}$ \citep{DAndrea2012633} speed: the speed below which 85\% of the people drive.
The comfortable speed is computed by Equation~\ref{eq:vm} from \citep{Setra.2002}, where $R$ is the radius of the road curve.

\begin{equation}\label{eq:vm}
V_d = \dfrac{102}{1 + \dfrac{346}{R^{1.5}}}
\end{equation}


\paragraph{Reaction Time-based Collaborative Velocity Control (RT-CVC)} is proposed to help ego-vehicle to deal with the potential brake of previous vehicle \citep{Hao2017}.
This model is developed to deal with occupancy of a conflict zone
From the point of view of car following, the conflict zone could be considered as a location on road before the following vehicle.
This model is used as control algorithms into autonomous cars.
The acceleration $a_r$ of a vehicle is provided by Equation \ref{eq:rtcvc1}.
This acceleration is used by the following vehicle for braking or acceleration during the next simulation step $\Delta t$.

\begin{equation}\label{eq:rtcvc1}
a_r = \dfrac{b_f \tau - 2 v_f \pm 2 b_f \sqrt{\dfrac{b_f b_l \tau^2 + 4 b_l v_f \tau + 4 v_l^2 - 8 b_l s}{4 b_f b_l}}}{2\tau}
\end{equation}

Where, $s_0$ is the minimal distance headway between the follower and leader.
$s+s_0$ is the distance between the two successive vehicles.
$v_f$ and $v_l$ are the current velocity of the follower and leader, respectively.
$b_f$ and $b_l$ are the respective maximum deceleration for the follower and leader.
And, $\tau$ is the reaction time of the driver.

In some unusual cases, the bumper-to-bumper distance may be less than the minimal distance headway $s_0$.
Consequently, the final control equation is given by Equation \ref{rtcvc2}.

\begin{equation}\label{eq:rtcvc2}
a_r = \begin{cases}
b_f & \text{if } s < 0 \\
\dfrac{b_f \tau - 2 v_f - 2 b_f a^*}{2 \tau} & \text{if } s \ge 0 \\
\end{cases}
\end{equation}

Where:
\begin{equation}\label{eq:rtcvc3}
a^* = \sqrt{\dfrac{b_f b_l \tau^2 + 4 b_l v_f \tau + 4 v_l^2 - 8 b_l s}{4 b_f b_l}}
\end{equation}

\paragraph{Intelligent Driver Model (IDM)} is a time-continuous car following model for the simulation of freeways and urban traffic \citep{PhysRevE.62.1805}.
It describes the dynamic of the positions and velocities of a vehicle, as defined in equations \ref{eq:idm} and \ref{eq:idm2}.

\begin{equation}\label{eq:idm}
\dfrac{dv}{dt} = a \cdot \left[ 1 - \left(\dfrac{v_\alpha}{v_0}\right)^4 - \left( \dfrac{s^*(v_\alpha, \Delta v_\alpha)}{s} \right)^2 \right]
\end{equation}

\begin{equation}\label{eq:idm2}
s^*(v_\alpha, \Delta v_\alpha) = s + v_\alpha T + \dfrac{v_\alpha \Delta v_\alpha}{2 \sqrt{ab}}
\end{equation}

$a$ is the comfortable acceleration, $v_\alpha$ is the current
speed for the vehicle $\alpha$, $v_0$ is the desired speed, $s_0$ is the minimum headway (at standstill).
$T$ is the desired time headway.
$\Delta v_\alpha$ is the speed difference with the leader.
$s$ is the current distance headway and $b$ is the comfortable deceleration.
The IDM shows realistic shockwave patterns but has a macroscopic capacity of just below $1,900$ veh/h.
In order to reach a reasonable capacity, the desired time headway needs to be lowered to unreasonable values. 

The initial model is adapted to discrete time progression by evaluating one of the two following equations at each time step: the free road $\dot{v}^{\text{free}}_\alpha$, and the interaction $\dot{v}^{\text{int}}_\alpha$ terms, which are detailed by Equations~\ref{eq:freeidm} and~\ref{eq:intidm}, respectively. The first one is used when the distance to the next obstacle is superior to a certain threshold, which is equivalent to an absence of an obstacle. The second term is used when there is an obstacle close enough to have to adapt the speed according to this obstacle.

\begin{equation}\label{eq:freeidm}
\dot{v}^{\text{free}}_\alpha = a \left( 1 - \left( \dfrac{v_\alpha}{V_\alpha} \right)^\delta \right)
\end{equation}

\begin{equation}\label{eq:intidm}
\dot{v}^{\text{int}}_\alpha = - a \left( \dfrac{s_0 + v_\alpha T}{s} + \dfrac{v_\alpha \Delta v_\alpha}{2 s \sqrt{ab}} \right)^2
\end{equation}

$V_\alpha$ is the maximum speed of the vehicle $\alpha$.
$\delta$ is a constant usually set to $4$.


\paragraph{Intelligent Driver Model Plus (IDM+)} is an variant of IDM in order to focus on the traffic flow stability \citep{Schakel10}.
To this end, a minimization over the free-flow and the interaction terms of equations \ref{eq:intidm} and \ref{eq:intidm} is defined, similarly to models based on \citep{Helly1961} and \citep{Gipps1981}, as described in equations \ref{eq:idmplus} and \ref{eq:idm2}.

\begin{equation}\label{eq:idmplus}
\dfrac{dv}{dt} = a \cdot \min \left[ 1 - \left(\dfrac{v_\alpha}{v_0}\right)^4, 1 - \left( \dfrac{s^*(v_\alpha, \Delta v_\alpha)}{s} \right)^2 \right]
\end{equation}

By explicitly separating the free-flow and interaction terms, the equilibrium fundamental diagram of the IDM changes from a smooth topped-off shape to a triangular shape.
Unstable behavior in the IDM is largely dependant on $s^*$ as this
includes the exaggerated response to speed difference and deviation from the equilibrium headway.
Strong decelerations triggering traffic flow instability occur with $s^* >> s$.
This still holds for the IDM+ as long as $v \le v_0$.
The maximum acceleration difference is than equal to $a$. 

\subsubsection{General Driver Behavior}

The behaviour of a road vehicle driver is based on the layered architecture presented in Figure of \figref{agent_layer} page \figpageref{agent_layer}.
The articulation between the components of these layers is illustrated by Figure \figref{vehicle_behaviour} and they are described in the following sections.

\mfigure{width=\linewidth}
{vehicle_behaviour}
{General architecture of the car driver behaviors}
{vehicle_behaviour}

\paragraph{Path Selection:} In this module, the agent chooses the route to follow in order to reach its goal.
A sequence of daily activities, in which travel activities are involved, may be modeled and used.
As explained in the ``caution'' note above, the modeling of the daily activities is part of the strategic layer, and outside the scope of this project.
Therefore, the routes are pre-calculated and provided to agents following a stochastic distribution, arising directly from the results of the macroscopic or microscopic simulations, or from a path computation model (see Section \ref{section:pathcomputation}).

\paragraph{Trajectory and Speed Computation:} The choice of cruising speed, short-term steering of the vehicle (allowing the wheels to be steered) and the nearest obstacle to be considered (whether it is a vehicle or a traffic sign) must be made along selected route.
\begin{description}
\item[Desired/Cruise Speed Selection] The choice of the desired/cruise speed is achieved by modulating the maximum speed allowed by the regulations, and expressed through road signals.
Therefore, the module for trajectory and speed computation analyzes the percepts of the agent in order to detect any change in this speed information.
When the maximum legal speed change is effective, a number in an interval set around this limit is randomly generated and becomes the agent's preferred cruising speed.
The interval is defined as a percentage around the maximum speed.
For example, we can define that a normal driver will choose a cruising speed between 90\% and 100\% of the maximum allowed speed.
In order to model human behaviors, it is recommended to set the upper limit of this range to a value higher than 100\%.
In this way, it is possible to simulate drivers that are exceeding speed limit.

\mfigure{width=.5\linewidth}
{curve_speed}
{Road curbature illustration in order to adapt the desired/cruise speed of the agent}
{curve_speed}

\item[Trajectory Management] The module for trajectory and speed computation should also choose the short-term direction of the vehicle.
To do this, the $P_c$ point projected on the central axis of the \emph{lanes} along the road at the front of the vehicle at a given distance is used as a target \citep{Reynolds.99}, as illustrated on Figure \figref{curve_speed}.
This method eanbles any vehicle agent to anticipate a curve and not skid out of a turn as long as the vehicle's speed is reasonable.
We recommend a distance of between 5 and 10 meters at the front of the vehicle in order to obtain realistic results from the simulations.

In Reality, in order to avoid any skid of the vehicle during a very tight turn, it is necessary to reduce the cruising speed chosen by the driver.
To model this driving behavior, a point $P_t$ is projected on the central axis of the road, in the same way as for the determination of $P_c$ previously.
The speed is then reduced based on the angle $\alpha$ between the current steering direction of the vehicle $\hat{z}$ and the direction to the point $P_t$.
The smaller the cosinus is, the higher the cruising speed should be reduced.
This relation is expressed into Equation \ref{eq:speedreduction}, where $\dot{v}$ and $\dot{v}'$ are the speeds before and after the adaptation to the trajectory, respectively. $\theta$ is the factor for modeling the approximate computation of a speed that is done by a human. Its value is randomly selected in the range $[1-\rho ; 1+\rho]$ with $\rho \in [0;1)$ a calibration factor.
Each driver agent has the choice to adapt or not its speed to the road trajectory.
This fact is modeled with a probability that should be greater than or equal to $p_{as}$.

\begin{equation}\label{eq:speedreduction}
\dot{v}' = \begin{cases}
	\left(\theta \cos \alpha\right) . v & \text{if } P(\text{change}) \ge p_{as} \\
	\dot{v} & \text{otherwise}
\end{cases}
\end{equation}

In our past simulations, a distance of 50 meters is used to determine the point $P_t$.
This value is suitable for urban simulation, i.e. at a speed of less than 50 km/h.
In the case of simulations of rural area or highway, it is reasonable to double or even triple this value, so that the driver anticipates the curves further.
Highway is a special case in which the impact of the curves is low because most of the highways are designed in order to limit road curvatures \citet{Staplin2001}.

\item[Lane Change] When the vehicle has to change of lane in order to follow a route, the trajectory and speed calculation module changes the direction of the vehicle and chooses the nearest other vehicle to follow.
The followed vehicle, if there is one, is the closest one to the current vehicle on the current lane or the destination one. 

Before changing of lane, the driver must check that the destination lane is clear and without obstacle.
Many authors have proposed models that allow the driver to make this decision by estimating the dangerousness of the manoeuvre \citep{Ahmed1999,Choudhury2005,Ehmanns2000}.
We recommend to use the model of \citep{Gipps1986}, which is a reference model from scientific literature and in simulation software.

To adjust the direction of the vehicle, the same projection method than previously is used in order to compute the target point $P_c$, except that $P_c$ is on the destination lane instead of the current lane.

\begin{upmcaution}
	There is no lane change module implemented into the provided simulator.
	The implemented model of the driver agent calls its ``lane change'' capacity.
	This capacity is currently implemented with an agent skill that does nothing.
	Nevertheless, the lane change behavior could be easily added with a specific ``skill'' implementation.
\end{upmcaution}


\item[Road Sign Support] It is important to consider (smart or not) road signs in the traffic simulation, particularly speed-limit panels.
The module for road sign management determines whether the vehicle is allowed to extract information from the perceived signs, and updates the knowledge of the agent.

\end{description}

\paragraph{Collision Avoidance:} This module is responsible for issuing the agent influences for the driver agent.
It uses the vehicle definition (its body, see Section \ref{section:bodymind} page \pageref{section:bodymind}) to emit the influences, and provides a information such as the desired acceleration.

This module is mostly based on car following models \citep{Chandler1958,Gipps1981,PhysRevE.62.1805,Bevrani2012}.
The Intelligent Driver Model (IDM), proposed by \citet{PhysRevE.62.1805} is the choosen one because it is a standard model in road traffic simulation.
Moreover, the model's parameters could be easily calibrated (see Section \ref{section:drivingacceleration} for details).

\subsubsection{Support of the foggy weather conditions}

Fog is among the most influential factors that impact road traffic safety due to drastic effect on reducing drivers' visibility and its ability to alter driving behavior.
The nexus between reduced visibility, impaired driving conditions, reckless driving behavior, low response times and cascaded fatal pileups has been a major source of social, economic and public health concerns.
Consequently, the question of the support of foggy weather conditions in the simulation model arise.

Foggy weather conditions should be included into the simulation in two different modules:
\begin{enumerate}
	\item[Fog in the environment] Fog has a real presence in the environment.
		Therefore, it is included into the environment's model, as described in Section \ref{section:environmentobjects} page \pageref{section:environmentobjects}.
		Consequently, the agent becomes able to perceive fog by extracting \texttt{FogWall} objects from the collection of objects that it is perceiving around.
		The position and the distance to this wall of fog could be used by the driver agent to determine its best speed.
	\item[Fog within the driving behavior] Assuming that the driver agents are able to perceive fog, as described in the previous point, the driving behavior should be adapted by considering the fog wall as a possible obstacle.
		Consequently, only the \emph{collision avoidance module} is impacted by foggy weather situation in our driving behavior architecture.
		In this case the collision avoidance model select the ``most dangerous'' object between a car to follow and the fog wall.
		Dangerousity of the objects is based on the distances to these objects.
		Therefore, the most dangerous object $o$ in the set of perceived objects $\mathbb{O}$ is the closest one:
		\begin{equation}
			\exists o \in \mathbb{O}, \forall i \in \mathbb{O} / distance(o) \le distance(i)
		\end{equation}
		When $o$ is a fog wall, the relative speed to $o$ is always $0$ and the relative distance to $o$ is proportional to the fog density, which is expressed as the perception distance of the fog into the environment model (Section \ref{section:environmentobjects} page \pageref{section:environmentobjects}).
\end{enumerate}

\subsection{Connected Driver}

A connected car is a car that is equipped with Internet access, and usually also with a wireless local area network.
This allows the car to share internet access with other devices both inside as well as outside the vehicle.
Often, the car is also outfitted with special technologies that tap into the internet or wireless LAN and provide additional benefits to the driver.

\mfigure{width=.7\linewidth}
{its}
{Intelligent Transport System composed of Connected Infrastructure and Connected Vehicles}
{its}

As illustrated by Figure \figref{its}, this communication capability may be based on two types of communication: V2I and V2V.

\emph{Vehicle-to-infrastructure} (V2I) communication is the two-way exchange of information between cars, trucks and buses and traffic signals, lane markings and other smart road infrastructure via a wireless connection. 
The overall goals of vehicle to infrastructure technology are to improve road safety and reduce collisions and support work zone and traffic management. 

Sensors carry out important functions in today’s vehicles. Ultrasonic, radar and camera technologies allow vehicles to see and analyze their surroundings and make safe decisions while driving. 
However, sensors have limited range and run into the same problems as humans do (although they react much quicker) when it comes to hidden objects, roads, and generally unexpected behavior from other vehicles. \emph{Vehicle-to-vehicle} (V2V) communications systems aims to correct this weakness by letting cars speak with one another directly and share information about their position, speed, and status. 
Left turn assist and intersection movement assist are two V2V technologies aimed at improving road safety. These technologies will warn the driver of a potential collision when making a left turn or entering an intersection.

V2X communication, or \emph{vehicle-to-everything}, combines V2I and V2V and extends those benefits to others on the roadway.
The idea behind this technology is that a vehicle with built-in electronics will be able to communicate in real-time with its surroundings including V2I and V2V, vehicle-to-pedestrian (V2P) and vehicle-to-network (V2N). V2X functions include \citep{Siam2019}:
\begin{itemize}
	\item Informing autonomous vehicles of out-of-sight vehicles;
	\item Warning distracted pedestrians of oncoming traffic;
	\item Delivering alerts for weather and road conditions to drivers;
\end{itemize}

\emph{Vehicle-to-network} (V2N) systems connect vehicles to cellular infrastructure and the cloud so drivers can take advantage of in-vehicle services like traffic updates and media streaming.
Some of the most common examples of this technology are vehicles with built-in traffic and navigation functions like Google Maps or Waze, or vehicles that can sync with a smartphone to play music.

\emph{Infrastructure-to-everything (I2X)} In the context of this project, we also define the communication from the instratructure to the infrastructure (I2I) or the vehicles (I2V), both are part of the Infrastructure-to-everything (I2X) communication.
These communication types are used in order to clearly separate the validation scenarios in Chapter \ref{section:experimentscenarios}.

\paragraph{Adaptation of the Agent Behavior for Connected Cars:} The behavior of the driver agent us updated in order to include the support of the different types of communications, that are explained above.

Basically, the communication model is based on the communication capabilities of the agents.
As explained in Chapter \ref{section:janussarl}, agents exchange data with events.
Consequently, each time an agent is receiving an event, it means that it is receiving a message from another connected object.
Additionally, when an agent wants to broadcast a data to the other connected objects, it has to build and emit an event.

\mfigure{width=\linewidth}
{connected_vehicle_behaviour}
{General architecture of the connected car driver behaviors}
{connected_vehicle_behaviour}

As illustrated by Figure \figref{connected_vehicle_behaviour}, two new modules have been added into the general architecture of the driver agents.
\begin{description}
	\item[Communication receiver] This module is in charge of receiving and filtering the events that are receiving by the agent.
		Its role is to extract any relevant data from the events in order to update the knowledge of the agent.
		
		The events that are considered in this project are:
		\begin{itemize}
			\item \code{Speed Limit}: this event contains a speed limit that should not be overtaken by the receiving agent.
			
			When an agent receives an occurrence of this event, it is updating its knowledge about the maximal speed that it could select.			
			\item \code{Dangerous Situation} this event contains the position and the type (accident, etc.) of a dangerous situation.
			
			When an agent receives an occurrence of this event, it includes the situation as a candidate for avoiding collision.
		\end{itemize}
	\item[Information emitter] This module fires events into the system in order to set up a communication between the agents.
	
	The module is based on the algorithm in Listing \ref{informationemittermodule}.
	The time $t'$ at which the events should be received in randomly computed into the time windows $[0; \rho]$, where $\rho$ is a parameter of the agent model, i.e. each agent could have a different value for $\rho$.
	The agent $a$ emits a \code{SpeedLimit} event according to its probability $p_{smax}$.
	This event contains the known maximal speed limit $\dot{v}_{smax}$, the timestamp $t'$ and the current position $pos_a$ of the agent $a$.
	If the agent $a$ knows the position $pos_{dang}$ of a dangerous situation, it emits with probability $p_{dang}$ an event of type \code{DangerousSituation} with $pos_{dang}$, the timestamp $t'$, and its current position $pos_a$.
\end{description}

\begin{lstlisting}[caption={Algorithm of the Information Emitter Module}, label={informationemittermodule}, frame=lines, xleftmargin=0.7cm, xrightmargin=0.5cm]
var t' = t + [0..(*@$\rho$@*).seconds].random
if P(p (*@$\le p_{smax}$@*)) {
   emit(new SpeedLimit((*@$\dot{v}_{smax}$@*), t', (*@$pos_a$@*)))
}
if (*@$pos_{dang} \neq \emptyset$@*) & P(p (*@$\le p_{dang}$@*)) {
   emit(new DangerousSituation((*@$pos_{dang}$@*), t', (*@$pos_a$@*)))
}
\end{lstlisting}

\begin{upminfo}
	All the events that are related to V2X communication are timestamped with the date of emission, and the date of reception.
	This information is used by the simulator in order to determine when an event should be delivered to the agent.
	The delivery time does not dependent on spatial conditions.
	In order to have a dynamic delivery time, it is necessary to update the simulator implementation.
\end{upminfo}

\section{Road signs' behaviors}


Vehicle-to-Infrastructure (V2I) is part of Intelligent Transportation Systems (ITS).
V2I technologies capture vehicle-generated traffic data, wirelessly providing information such as advisories from the infrastructure to the vehicle that inform the driver of safety, mobility, or environment-related conditions.

The infrastructure components have three capabilities \citep{Farah2017}:
\begin{description}
	\item[Sensing the environment] They are known as sensors. The purpose is to scan the environment (dynamically or passively) in order to extract data.
	They could be of different type (image-based, signal-based, etc.).
	The data could be used for updating the component's state, or could be deliver through a communication.
	\item[Communicating with other components] Infrastructure components may set up a communication with other infrastructure components or with the Network.
	This communication makes the component able to deliver the sensor data to other parties.
	\item[Communicating with vehicles] This is a specific type of local communication, in which the other party is a vehicle.
\end{description}

\subsection{General Behavior}

The behavior of an infrastructure component, and the associated three capabilities are dirrectly supported by an intelligent agent.
The behavior of this type of agent is illustrated by the diagram on Figure \figref{roadsign_behavior}.

\mfigure{width=.8\linewidth}
{roadsign_behavior}
{General architecture of the road signs' behavior}
{roadsign_behavior}

Five modules contribute to the general behavior of an infrastucture component:
\begin{description}
	\item[Data extraction] This module is dedicated to the sensing of the environment and the extraction of raw data.
		Usually in the context of simulation, the algorithms inside this module are not the same algorithms that are deployed in real sensors: they are approximate behaviors.
		The role of this module is to specify the field of perception of the sensor in order to make the environment modules to compute the set of objects in this field (see Chapter \ref{section:environmentdomain}).
		The set $D_r$ of extracted data is composed by objects $o$ that have their position $pos_o$ in the field of view $view_a$ of the agent $a$.
		\begin{equation}
		D_r = \left\{o | o \in \mathbb{O}, pos_o \in view_a \right\}
		\end{equation}
	\item[Data filtering] The role of this module is to discard the objects for the extract data set that are not relevant to the behavior of the infrastructure component.
	The set $D_f$ of filtered data is composed by objects $j$ that were extracted and of a relevant type, where $R_a$ is the set of the types of data that are useful for the agent behavior.
		\begin{equation}
		D_f = \left\{ j | j \in D_r, type(j) \in R_a \right\}
		\end{equation}
	\item[State Change Computation] This module is the key module of an instracture agent.
		Its role is to update the variables in the current object's state $\Delta$ according to the filtered data $D_f$.
		\begin{equation}
		\Delta' = updateState(\Delta, D_f)
		\end{equation}
		For example, the state of a traffic light may be a value in the set $\{Pass, Prepare to stop, Stop \}$.
	\item[Physic State] This module is in charge of transforming the new state $\Delta'$ of the infrastructure component into influences that will change the physical description of this component into the environment (see Chapter \ref{section:environmentdomain}).
		For example, the module for a traffic light may transform the state value $Pass$ to the green color, $Prepare to stop$ to orange, and $Stop$ to red.
	\item[Communication Emitter] This module is in charge to prepare any communication with other infrastructure component (I2I), the Network (I2N), or vehicle (V2I).
	As for driver agents, the communication is supported by agent events (see Chapter \ref{section:janussarl}).
	At this stage of the behavior specification, it is impossible to determine the types of events and the embedded data:
	they are specific to the type of infrastructure component, see Section \ref{section:speedlimitpanelbehavior} for a detailed description of the behavior of a smart speed limit panel.
	Nevertheless, the events are built upon the filtered data $D_f$ directly, or the new state $\Delta'$ of the component.
\end{description}

\subsection{Smart Speed Limit Panel}\label{section:speedlimitpanelbehavior}

In the context of this project, a single type of infrastructure agent is modeled and implemented: smart speed limit panel.
The general behavior of this agent is to show up the current speed limit at the location of the panel.
This speed limit is computed according to Equation \ref{eq:panelspeedlimit}, where $\dot{v}_{legal}$ is the legal speed limit (constant into the agent behavior) and $\dot{v}_{percept}$ is the speed limit that is the result of the data extracted by the agent.
\begin{equation}\label{eq:panelspeedlimit}
	\dot{v}_{max} = \min\left( \dot{v}_{legal}, \dot{v}_{percept} \right)
\end{equation}

According to Equation \ref{eq:speedlimitpercept}, the perceived speed limit $\dot{v}_{percept}$ depends on two inputs:
\begin{itemize}
	\item the speed limit $\dot{v}_{v2i}$ that is embedded into a \code{SpeedLimit} event, received by the agent; and
	\item the fog sensor, that provides information about the fog density at the place of the panel. This density is used for computing the expected speed limit $\dot{v}_{fog}$ in the foggy weather condition.
\end{itemize}
\begin{equation}\label{eq:speedlimitpercept}
	\dot{v}_{percept} = \min\left( \dot{v}_{v2i}, \dot{v}_{fog} \right)
\end{equation}

The fog-dependent speed limit is computed according to:
\begin{equation}
	\dot{v}_{fog} = \begin{cases}
		+\infty & \text{if no fog around} \\
		\dfrac{d_{fog} . \dot{v}_{legal}}{m_{fog}} & \text{if fog}
	\end{cases}
\end{equation}
Where, $d_{fog} \in \R+$ is the current visibility distance into the fog (representation of the fog's density), and $m_{fog} \le d_{fog}$ is the maximal visibility distance into the fog under which the fog's impact on the traffic exists.

Finally, the speed $\dot{v}_{max}$ is displayed on the panel, and sended via communication channel to the neighbor objects (vehicles or other speed limit panels).

\end{graphicspathcontext}

