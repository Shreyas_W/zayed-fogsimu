\chapterfull{Specification of the File Format for Scenario Description}{File Format for Scenario Description}\label{section:simulationscenationdescription}

\begin{graphicspathcontext}{{./img/architecture/},\old}

In this chapter, the format of the files that are used for describing the simulation scenarios is described. A scenario file is a file following the XML\footnote{XML file format: \url{https://en.wikipedia.org/wiki/XML}} file format that is described below.
Concrete examples of XML scenario files could be found in annex chapters \ref{section:configurationfile:scenario1} (page \pageref{section:configurationfile:scenario1}),  \ref{section:configurationfile:scenario2} (page \pageref{section:configurationfile:scenario2}), \ref{section:configurationfile:scenario3} (page \pageref{section:configurationfile:scenario3}), and \ref{section:configurationfile:scenario4} (page \pageref{section:configurationfile:scenario4}).

This chapter describes the syntax of a XML scenario file by presenting the Document Type Definition\footnote{DTD: \url{https://en.wikipedia.org/wiki/Document_type_definition}} (DTD).

\section{Root Section}

All XML files must start with a root node. In the scenario file, the root node is \code{scenario} as described into the following DTD:
\begin{lstlisting}[language=XML]
<!ELEMENT scenario (preferences?, time?, spawners, probes?, charts?,
                    roadNetwork, layers?, roadObjects?,
                    fogZones?)>
<!ATTLIST scenario
          name CDATA #IMPLIED>
\end{lstlisting}

The \code{scenario} root node contains six nodes that are described in the following sections: \code{roadNetwork}, \code{time}, \code{layers}, \code{roadObjects}, \code{fogZones}, \code{spawners}, \code{probes}.

The \code{scenario} root node has the following attribute:
\begin{description}
	\item[name] the name of the scenario. It is a freely type string of characters.
\end{description}

\section{General Preferences}

The simulator provides a set of general preferences that could be edited with the dedicated graphical user interface.
Nevertheless, a specific simulation scenario may have to define a specific set of values for these preferences.
In order to enable to overriding the general preferences that are editable into the user interface, it is possible to specify the values of the overridden preferences into the scenario XML file, with the \code{preferences} node.
The specification of the \code{preferences} node is in the following DTD:
\begin{lstlisting}[language=XML]
<!ELEMENT preferences (NAME+)>
<!ELEMENT NAME (#DATA)>
\end{lstlisting}

The \code{NAME} nodes are child nodes of \code{preferences}, such that:
\begin{itemize}
	\item the name of the \code{NAME} node is the name of the preference value to be overridden.
	\item the text value that is specified into the \code{NAME} node is the new value for the preference.
\end{itemize}

For example, let the following definition:
\begin{lstlisting}[language=XML]
<scenario>
	<preferences>
		<name1>value1</name1>
		<name2>value2</name2>
	</preferences>
</scenario>
\end{lstlisting}
It defines the value \code{value1} (resp. \code{value2}) for the preference identifier \code{name1} (resp. \code{name2}).

The provided names for the values should corresponds to a name of a value into the general preferences of the simulator.

\section{Time Management}

As for all the simulator, it is possible to specify how the time is evolving during a simulation.
The \code{time} node enables the specification of the time management.
The specification of the \code{time} node is in the following DTD:
\begin{lstlisting}[language=XML]
<!ELEMENT time #EMPTY>
<!ATTLIST time
          start CDATA #IMPLIED
          step  CDATA #IMPLIED
          stop  CDATA #IMPLIED
          delay CDATA #IMPLIED>
\end{lstlisting}

The \code{time} node has the following attribute:
\begin{description}
	\item[start] the value of the first time to be simulated (\code{start}$>=0$).
	\item[step] the size of one simulation step, i.e. the quantity of simulated time between two runs of the simulator (\code{step}$>0$).
	\item[stop] when it is specified, the simulation automatically stops at this given time (\code{stop}$>=$ \code{start}).
	\item[delay] it is a small delay (in milliseconds) that is applied before running a simulation step (\code{delay}$>=0$). This parameter enables to slowdown artificially the running performance of the simulator.
\end{description}

If the attributes are not provided, the values are read from the general preferences of the application.


\section{Spawners}

A spawner is a component of the simulator that is able to create simulated agents during the simulation process.
The \code{spawners} node permits to specify where the spawners are located, and what they are generated.
The specification of the \code{spawners} node is in the following DTD:
\begin{lstlisting}[language=XML]
<!ELEMENT spawners (spawner*)>
\end{lstlisting}
It consists of a list of \code{spawner} nodes.

\subsection{Spawner Node}

The \code{spawner} node specifies a single spawner following the DTD:
\begin{lstlisting}[language=XML]
<!ELEMENT spawner (law?, bodyParameters?, agentParameters?)>
<!ATTLIST spawner
          enable CDATA #IMPLIED "true"
          id     CDATA #IMPLIED
          x      CDATA #REQUIRED
          y      CDATA #REQUIRED
          agent  CDATA #REQUIRED
          body   CDATA #IMPLIED
          budget CDATA #IMPLIED>
\end{lstlisting}

The \code{spawner} node has the following attribute:
\begin{description}
	\item[enable] it indicates if the spawner is enabled (if the value is \code{true}) or disabled (if the value is \code{false}). If the spawner is enabled, the spawner generates agents, otherwise it is not generating agents (\code{enable} $\in \{true, false\}$).
	\item[id] the identifier of the spawner. The value should be a valid UUID\footnote{UUID: \url{https://en.wikipedia.org/wiki/Universally_unique_identifier}}. Otherwise, a valid UUID will computed from the given value.
	\item[x] the X coordinate of the position of the spawner on the map. The coordinate system is the one of the geographical information system related to the map.
	\item[y] the Y coordinate of the position of the spawner on the map. The coordinate system is the one of the geographical information system related to the map.
	\item[agent] the fully qualified name of the SARL/Java class that implements the agents to be spawn.
	\item[body] the fully qualified name of the SARL/Java class that implements the agent body to be spawn. If it is not provided, the default body's type is used.
	\item[budget] The maximum number of agents to be spawned. If it is not provided, the spawner will generate an infinite number of agents (\code{budget}$\ge 0$).
\end{description}


\subsubsection{Spawning Law Node}\label{section:dtd:lawnode}

The number of agents that are generated per second depends on a stochastic law that is defined into the \code{law} node.
This law is used to compute randomly the number of vehicle per hours.
From this number, the spawner deduces the number of vehicles to be spawner at a specific time $t$.
The specification of the \code{law} node is in the following DTD:
\begin{lstlisting}[language=XML]
<!ELEMENT law (lawParam*)>
<!ATTLIST law
          name CDATA #REQUIRED>
\end{lstlisting}

The \code{law} node has the following attribute:
\begin{description}
	\item[name] it is the name of the stochastic law that must be used for the specification of the generation rate. The name is used to build a fully qualified name of the SARL/Java class in which the law is implemented. The available names are described into Table \tabref{stochasticlawnames}.
\end{description}

\begin{mtable}{\linewidth}{3}{|l|X|l|}{Names of the stochastic laws into the scenario file}{stochasticlawnames}
	\tabularheader{Name}{Fully qualified name}{Section/Page}
	\code{bernoulli} & \code{org.arakhne.afc.math.stochastic.BernoulliStochasticLaw} & \ref{section:nonuniformrandom:bernoulli}, pp. \pageref{section:nonuniformrandom:bernoulli} \\
	\hline
	\code{cauchy} & \code{org.arakhne.afc.math.stochastic.CauchyStochasticLaw} & \ref{section:nonuniformrandom:cauchy}, pp. \pageref{section:nonuniformrandom:cauchy} \\
	\hline
	\code{constant} & \code{org.arakhne.afc.math.stochastic.ConstantStochasticLaw} & \ref{section:nonuniformrandom:uniform}, pp. \pageref{section:nonuniformrandom:uniform} \\
	\hline
	\code{exponential} & \code{org.arakhne.afc.math.stochastic.ExponentialStochasticLaw} & \ref{section:nonuniformrandom:exponential}, pp. \pageref{section:nonuniformrandom:exponential} \\
	\hline
	\code{gaussian} & \code{org.arakhne.afc.math.stochastic.GaussianStochasticLaw} & \ref{section:random:normal}, pp. \pageref{section:random:normal} \\
	\hline
	\code{linear} & \code{org.arakhne.afc.math.stochastic.LinearStochasticLaw} & \ref{section:stochasticlaw:linear}, pp. \pageref{section:stochasticlaw:linear} \\
	\hline
	\code{logistic} & \code{org.arakhne.afc.math.stochastic.LogisticStochasticLaw} & \ref{section:stochasticlaw:logistic}, pp. \pageref{section:stochasticlaw:logistic} \\
	\hline
	\code{logNormal} & \code{org.arakhne.afc.math.stochastic.LogNormalStochasticLaw} & \ref{section:stochasticlaw:lognormal}, pp. \pageref{section:stochasticlaw:lognormal} \\
	\hline
	\code{pareto} & \code{org.arakhne.afc.math.stochastic.ParetoStochasticLaw} & \ref{section:stochasticlaw:pareto}, pp. \pageref{section:stochasticlaw:pareto} \\
	\hline
	\code{triangular} & \code{org.arakhne.afc.math.stochastic.TriangularStochasticLaw} & \ref{section:stochasticlaw:triangular}, pp. \pageref{section:stochasticlaw:triangular} \\
	\hline
\end{mtable}

Each of the laws that are described in Table \tabref{stochasticlawnames} has a collection of parameters in order to be initialized.
The \code{law} node contains a node for each initialization parameter.
The name of the node is the name of the expected parameter.
The value of the node is the value to give to the parameter.
For example, the parameters named \code{mean} and \code{standardDeviation} for the \code{guassian} law are specified with the concrete values \code{12345} and \code{6789} as:
\begin{lstlisting}[language=XML]
<law name="gaussian">
	<mean>12345</mean>
	<standardDeviation>6789</standardDeviation>
</law>
\end{lstlisting}

\subsubsection{Parameters for the Spawned Body}

It is possible to pass constant values to the instances of agent bodies that are created by the spawner.
This list of body's parameters is specified into the \code{bodyParameters} of the \code{spawner} node.
The \code{bodyParameters} node specifies a single spawner following the DTD:
\begin{lstlisting}[language=XML]
<!ELEMENT bodyParameters (minSpeed?, maxSpeed?,
                          initialLineLaw?)>
\end{lstlisting}

When the vehicle (the agent body) is generated, it may already have a specific speed.
In order to specify this initial speed, the \code{minSpeed} and \code{maxSpeed} nodes may be used.
The initial speed will be selected randomly (with an uniform stochastic law) between \code{minSpeed} and \code{maxSpeed}.
The \code{minSpeed} nodes specifies the minimal speed (m/s) to pass to the body.
The \code{maxSpeed} nodes specifies the maximal speed (m/s) to pass to the body.
For example:
\begin{lstlisting}[language=XML]
<bodyParameters>
	<minSpeed>0</minSpeed>
	<maxSpeed>10</maxSped>
</bodyParameters>
\end{lstlisting}
If the \code{minSpeed} node is not specified, then the generated vehicle will be immobile at the time of irs generation.
If the \code{minSpeed} node is specified, but not the \code{maxSpeed} node, then the vehicle will have the speed specified in \code{minSpeed}.
If both \code{minSpeed} and \code{maxSpeed} nodes are given, the speed will be randomly selected between the associated values.

The \code{initialLineLaw} nodes permits to select the road lane on which the vehicle (the agent body) will be created.
Let the lanes numbered from the index $0$ (the right-most lane in continental Europe for example, or the left-most lane in UK) to $n-1$, where $n$ is the number of lanes for the road.
The stochatic law that is specified in the \code{initialLineLaw} nodes permits to select randomly on of the lane indexes.
Then, the selected index will be considered as the index of the lane on which the new vehicle will move.
The \code{initialLineLaw} has the same definition as the \code{law} node in Section \ref{section:dtd:lawnode}.


\subsubsection{Parameters for the Spawned Agent}

It is possible to pass constant values to the instances of agents that are created by the spawner.
The real use of parameters depends on the implementation of the agent (in SARL programming language).
This list of agent's parameters is specified into the \code{agentParameters} of the \code{spawner} node.
The \code{agntParameters} node specifies a single spawner following the DTD:
\begin{lstlisting}[language=XML]
<!ELEMENT agentParameters (byte | short | int | long | float |
                           double | char | string | boolean |
                           url | uri | uuid | file)*>
\end{lstlisting}

The different nodes that could be used for specifying a value are:
\begin{description}
	\item[byte] specifies an integer value on 1 byte, i.e. with a value in $[-128;127]$.
	\item[short] specifies an integer value on 2 bytes, i.e. with a value in $[-32,768;32,767]$.
	\item[int] specifies an integer value on 4 bytes, i.e. with a value in $[-2^{31};2^{31})$.
	\item[long] specifies an integer value on 8 bytes, i.e. with a value in $[-2^{63};2^{63})$.
	\item[float] specifies a single-precision 32-bit IEEE 754 floating point.
	\item[double] specifies a single-precision 64-bit IEEE 754 floating point.
	\item[char] is a single 16-bit Unicode character. It has a minimum value of \code{'\\u0000'} (or 0) and a maximum value of \code{'\\uffff'} (or $65,535$ inclusive).
	\item[string] is a string of 16-bit Unicode characters.
	\item[boolean] has only two possible values: \code{true} and \code{false}. 
	\item[uuid] is the string-representation of a Unisersal Unique IDentifier\footnote{UUID: \url{https://en.wikipedia.org/wiki/Universally_unique_identifier}} (UUID).
	\item[url] is the string-representation of an Uniform Ressource Locator\footnote{URL: \url{https://en.wikipedia.org/wiki/URL}} (URL).
	\item[uri] is the string-representation of an Uniform Ressource Identifier\footnote{URI: \url{https://en.wikipedia.org/wiki/Uniform_Resource_Identifier}} (URI).
	\item[file] is the string-representation of file path.
\end{description}

An example of XML code that is specifying agent parameters is:
\begin{lstlisting}[language=XML]
<agentParameters>
	<double>1.5</double>
	<uuid>a709d65f-d33e-4314-ba76-aa7e5829f7e0</uuid>
	<string>this is a string of characters</string>
</agentParameters>
\end{lstlisting}


\section{Probes}

It is important for a simulation to provide values that are extracted during the simulation runs.
The tools that enable to extract data are named ``probe''.
They are pieces of software code that implement the algorithms for extracted data.
The specification of the \code{probes} node is in the following DTD:
\begin{lstlisting}[language=XML]
<!ELEMENT probes (probe*)>
<!ATTLIST probes
          folder CDATA #IMPLIED
          basename CDATA #IMPLIED>
\end{lstlisting}

The \code{probes} node has the following attribute:
\begin{description}
	\item[folder] it is the name of the folder in which all the probed data are saved. It is not specified, the folder name \code{./probes} (with Unix notation, or \code{.\\probes} with Windows notation) will be used. This folder name is relative to the folder in which the scenario file is located.
	\item[basename] it is a simple name that will be used as the name of the sub-folder in which extracted data will be saved, i.e. if this attribute is specified, the data will be saved into the folder \code{<folder>/<basename>}, where \code{<folder>} and \code{<basename>} are the values of the attributes. If the \code{basename} attribute is not specified, a random name will be computed.
\end{description}


\subsection{Probe Node}

Each probe is specified into a \code{probe} node, following the DTD:
\begin{lstlisting}[language=XML]
<!ELEMENT probe (chartReference*)>
<!ATTLIST probe
          enable       CDATA #IMPLIED "true"
          type         CDATA #REQUIRED
          id           CDATA #IMPLIED
          name         CDATA #IMPLIED
          x            CDATA #IMPLIED
          y            CDATA #IMPLIED
          outputToFile CDATA #IMPLIED "true"
          fileBasename CDATA #IMPLIED
          enableCharts CDATA #IMPLIED "false">
\end{lstlisting}

\begin{mtable}{\linewidth}{2}{|l|X|}{List of the predefined probes that are application-independent}{predefinedprobelist}
	\tabularheader{Fully qualified name}{Short description}
	& \\
	\hline
	\multicolumn{2}{|l|}{\texttt{org.arakhne.afc.simulation.framework.framework1d.probes.def.}} \\
	\multicolumn{2}{|l|}{\texttt{GlobalIndividualInterVehicleDistanceProbe}} \\
	\cline{2-2}
	& Average distance between the cars on the roads, data is gathered for each car individually \\
	\hline
	\multicolumn{2}{|l|}{\texttt{org.arakhne.afc.simulation.framework.framework1d.probes.def.}} \\
	\multicolumn{2}{|l|}{\texttt{GlobalIndividualSpeedProbe}} \\
	\cline{2-2}
	&  Average speeds of the cars on the roads, data is gathered for each car individually \\
	\hline
	\multicolumn{2}{|l|}{\texttt{org.arakhne.afc.simulation.framework.framework1d.probes.def}} \\
	\multicolumn{2}{|l|}{\texttt{GlobalRoadDensityProbe}} \\
	\cline{2-2}
	& Density of cars on each road \\
	\hline
	\multicolumn{2}{|l|}{\texttt{org.arakhne.afc.simulation.framework.framework1d.probes.def}} \\
	\multicolumn{2}{|l|}{\texttt{GlobalRoadInterVehicleDistanceProbe}} \\
	\cline{2-2}
	& Average distances between the cars on each road. \\
	\hline
	\multicolumn{2}{|l|}{\texttt{org.arakhne.afc.simulation.framework.framework1d.probes.def}} \\
	\multicolumn{2}{|l|}{\texttt{GlobalRoadSpeedProbe}} \\
	\cline{2-2}
	& Average speeds of the cars for each road \\
	\hline
	\multicolumn{2}{|l|}{\texttt{org.arakhne.afc.simulation.framework.framework1d.probes.def.}} \\
	\multicolumn{2}{|l|}{\texttt{GlobalTravelDurationProbe}} \\
	\cline{2-2}
	& Average travel durations of the vehicles \\
	\hline
\end{mtable}
\begin{mtable}{\linewidth}{2}{|l|X|}{List of the predefined probes that are dedicated to the foggy weather application}{fogpredefinedprobelist}
	\tabularheader{Fully qualified name}{Short description}
	& \\
	\hline
	\multicolumn{2}{|l|}{\texttt{fr.ciadlab.fogsimu.probes}} \\
	\multicolumn{2}{|l|}{\texttt{FoggyIndividualInterVehicleDistanceProbe}} \\
	\cline{2-2}
	& Average distance between the cars on the roads under foggy weather condition, data is gathered for each car individually \\
	\hline
	\multicolumn{2}{|l|}{\texttt{fr.ciadlab.fogsimu.probes.}} \\
	\multicolumn{2}{|l|}{\texttt{FoggyIndividualSpeedProbe}} \\
	\cline{2-2}
	& Average speeds of the cars on the roads under foggy weather condition, data is gathered for each car individually \\
	\hline
	\multicolumn{2}{|l|}{\texttt{fr.ciadlab.fogsimu.probes.}} \\
	\multicolumn{2}{|l|}{\texttt{FoggyInterVehicleDistanceProbe}} \\
	\cline{2-2}
	& Average distances between the cars on each road under foggy weather condition. \\
	\hline
	\multicolumn{2}{|l|}{\texttt{fr.ciadlab.fogsimu.probes.}} \\
	\multicolumn{2}{|l|}{\texttt{FoggyRoadSpeedProbe}} \\
	\cline{2-2}
	& Average speeds of the cars for each road uder foggy weather condition \\
	\hline
\end{mtable}

The \code{probe} node has the following attribute:
\begin{description}
	\item[enable] it indicates if the probe is enabled (if the value is \code{true}) or disabled (if the value is \code{false}). If the probe is disabled, the probe does not gather data (\code{enable} $\in \{true, false\}$).
	\item[type] the fully qualified name of the SARL/Java class that implements the probe. Tables \tabref{predefinedprobelist} and \tabref{fogpredefinedprobelist} show the lists of the predefined probes into the simulator. You could also specify another type of probe if the corresponding SARL/Java class is added into the simulator project.
	\item[id] is the identifier of the probe. It is not a valid UUID, a valid UUID will be automatically generated from the string of characters that is specified into this attribute.
	\item[name] is the name of the probe. It may be a string of characters, or the identifier of a string of character into a property file that is inside the software.
	\item[x] if specified, the X coordinate of the position of the probe on the map. The coordinate system is the one of the geographical information system related to the map.
	\item[y] if specified, the Y coordinate of the position of the probe on the map. The coordinate system is the one of the geographical information system related to the map.
	\item[outputToFile] indicates if the data gathered by the probe are saved into a file (\code{outputToFile} $\in \{true, false\}$).
	\item[fileBasename] is the basename of the file that is created for saving the data. If it is not specified, the name of the file is built from the name of the probe's \code{type}. 
	\item[enableCharts] indicates if the simulation application activates the rendering of graphical charts for displaying the data gathered by the probe (\code{enableCharts} $\in \{true, false\}$).
\end{description}

The \code{chartReference} node permits to link the probe to charts that are defined into specified nodes in Section \ref{section:dtd:char}.
The DTD for a chart reference is:
\begin{lstlisting}[language=XML]
<!ELEMENT chartReference #EMPTY>
<!ATTLIST chartReference
          id CDATA #REQUIRED>
\end{lstlisting}
The \code{id} is the identifier of the chart that is defined in the XML file according to the specification in Section \ref{section:dtd:char}.



\section{Charts}\label{section:dtd:char}

The simulator is able to display the data extracted by the probes on graphical components.
These graphical components are named ``charts''.
In order to enable and specify the charts to display, the \code{charts} node is available, according to the DTD:
\begin{lstlisting}[language=XML]
<!ELEMENT charts (chart*)>
\end{lstlisting}

Each chart description is specified in a \code{chart} node:
\begin{lstlisting}[language=XML]
<!ELEMENT chart #EMPTY>
<!ATTLIST charts
          enable CDATA #IMPLIED "false"
          type   CDATA #REQUIRED
          id     CDATA #IMPLIED
          title  CDATA #IMPLIED
          ylabel CDATA #IMPLIED>
\end{lstlisting}

The \code{chart} node has the following attribute:
\begin{description}
	\item[enable] it indicates if the chart is enabled (if the value is \code{true}) or disabled (if the value is \code{false}). If the chart is enabled, the corresponding graphical component is added into the user interface (\code{enable} $\in \{true, false\}$).
	\item[type] specifies the type of chart to create. The value may be one of:
		\begin{description}
			\item[line] the data set will be drawn with lines.
		\end{description}
	\item[id] is the identifier of the chart. It is not a valid UUID, a valid UUID will be automatically generated from the string of characters that is specified into this attribute.
	\item[title] is the string of characters that will be displayed as the title of the chart.
	\item[ylabel] is the string of characters that will be displayed as the name of the vertical axis on the chart.
\end{description}



\section{Road Network}\label{section:dtd:roadNetwork}

The road network node specifies the ESRI Shape file that should be read for opening the map of roads.
The \code{roadNetwork} node is defined as:
\begin{lstlisting}[language=XML]
<!ELEMENT roadNetwork #EMPTY>
<!ATTLIST roadNetwork
          file CDATA #REQUIRED>
\end{lstlisting}

The \code{file} attribute represents the path to the ESRI Shape file to be read. The path is relative to the folder in which the scenario file is located.
An example of XML configuration is:
\begin{lstlisting}[language=XML]
<roadNetwork file="./shp/map-line2.shp"/>
\end{lstlisting}




\section{Layers}

As is the majority of the Geographical Information Systems (GIS), the simulator allows to structure the data into different layers.
Each layer is dedicated to a specific type of data, and associated to dedicated drawers.
The simulator application includes a layer for displaying the road network (see Section \ref{section:dtd:roadNetwork}), and those that are dedicated to the dynamic drawing of the simulated entities.

Additionally, it is possible to display more layers by defining a \code{layers} node:
\begin{lstlisting}[language=XML]
<!ELEMENT layers (layer*)>
\end{lstlisting}

Each layer must be specified into a dedicated \code{layer} node:
\begin{lstlisting}[language=XML]
<!ELEMENT layer #EMPTY>
<!ATTLIST layer
          enable CDATA #IMPLIED "true"
          file   CDATA #REQUIRED
          id     CDATA #IMPLIED
          name   CDATA #IMPLIED>
\end{lstlisting}

The \code{layer} node has the following attributes:
\begin{description}
	\item[enable] it indicates if the probe is enabled (if the value is \code{true}) or disabled (if the value is \code{false}). If the probe is disabled, the probe does not gather data (\code{enable} $\in \{true, false\}$).
	\item[file] is the path to the ESRI Shape file to be read. The path is relative to the folder in which the scenario file is located.
	\item[id] is the identifier of the layer. It is not a valid UUID, a valid UUID will be automatically generated from the string of characters that is specified into this attribute.
	\item[name] is the name of the layer.
\end{description}



\section{Road Objects}

The simulated environment contains objects of different types, including the road objects, e.g. road signs.
The node for specifying the objects to included into the simulated environment is \code{roadObjects}:
\begin{lstlisting}[language=XML]
<!ELEMENT roadObjects (group*)>
\end{lstlisting}

A bias is included into the simulator: the objects in the environment may be grouped in order to enable them to communicate together.
In order to enable this design architecture, the concept of group of objects in introduced.

A group of objects is a collection of environment objects with a shared communication space.
Therefore, the \code{group} node permits to describe a group of objects:
\begin{lstlisting}[language=XML]
<!ELEMENT group (roadObject*)>
<!ATTLIST group
          enable CDATA #IMPLIED "true"
          id     CDATA #IMPLIED
          name   CDATA #IMPLIED
          type   CDATA #IMPLIED>
\end{lstlisting}

The \code{group} node has the following attributes:
\begin{description}
	\item[enable] it indicates if the group is enabled (if the value is \code{true}) or disabled (if the value is \code{false}). If the group is disabled, none of the objects will be added to the simulated environment (\code{enable} $\in \{true, false\}$).
	\item[id] is the identifier of the group. It is not a valid UUID, a valid UUID will be automatically generated from the string of characters that is specified into this attribute.
	\item[name] is the name of the group.
	\item[type] fully qualified name of the SARL/Java class that must be used for creating the group's object into the simulator. The given type must be a subtype of \code{org.arakhne.afc.simulation.framework.base.environment.EnvironmentObjectGroup}.
\end{description}

Each environment object is described into a \code{roadObject} node:
\begin{lstlisting}[language=XML]
<!ELEMENT roadObject (agentParameters?)>
<!ATTLIST roadObject
          enable CDATA #IMPLIED "true"
          id     CDATA #IMPLIED
          x      CDATA #IMPLIED "0.0"
          y      CDATA #IMPLIED "0.0"
          type   CDATA #REQUIRED
          agent  CDATA #IMPLIED>
\end{lstlisting}

The \code{roadObject} node has the following attributes:
\begin{description}
	\item[enable] it indicates if the road object is enabled (if the value is \code{true}) or disabled (if the value is \code{false}). If the object is disabled, the object will be added to the simulated environment (\code{enable} $\in \{true, false\}$).
	\item[id] is the identifier of the group. It is not a valid UUID, a valid UUID will be automatically generated from the string of characters that is specified into this attribute.
	\item[x] the X coordinate of the position of the object on the map. The coordinate system is the one of the geographical information system related to the map.
	\item[y] the Y coordinate of the position of the object on the map. The coordinate system is the one of the geographical information system related to the map.
	\item[type] fully qualified name of the SARL/Java class that must be used for creating the object into the simulator. The given type must be a subtype of \code{org.arakhne.afc.simulation.framework.framework1d.environment.RoadObject}.
	\item[agent] fully qualified name of the agent type that must be used for creating the agent associated to the environment object.
\end{description}

When an \code{agent} is associated to an environment object, it is also possible to pass specific arguments to the agent when it is created by the simulator.
The \code{agentParameters} node contains the list of arguments:
\begin{lstlisting}[language=XML]
<!ELEMENT agentParameters (byte | short | int | long | float |
                           double | char | string | boolean |
                           url | uri | uuid | file)*>
\end{lstlisting}

The different nodes that could be used for specifying a value are:
\begin{description}
	\item[byte] specifies an integer value on 1 byte, i.e. with a value in $[-128;127]$.
	\item[short] specifies an integer value on 2 bytes, i.e. with a value in $[-32,768;32,767]$.
	\item[int] specifies an integer value on 4 bytes, i.e. with a value in $[-2^{31};2^{31})$.
	\item[long] specifies an integer value on 8 bytes, i.e. with a value in $[-2^{63};2^{63})$.
	\item[float] specifies a single-precision 32-bit IEEE 754 floating point.
	\item[double] specifies a single-precision 64-bit IEEE 754 floating point.
	\item[char] is a single 16-bit Unicode character. It has a minimum value of \code{'\\u0000'} (or 0) and a maximum value of \code{'\\uffff'} (or $65,535$ inclusive).
	\item[string] is a string of 16-bit Unicode characters.
	\item[boolean] has only two possible values: \code{true} and \code{false}. 
	\item[uuid] is the string-representation of a Unisersal Unique IDentifier\footnote{UUID: \url{https://en.wikipedia.org/wiki/Universally_unique_identifier}} (UUID).
	\item[url] is the string-representation of an Uniform Ressource Locator\footnote{URL: \url{https://en.wikipedia.org/wiki/URL}} (URL).
	\item[uri] is the string-representation of an Uniform Ressource Identifier\footnote{URI: \url{https://en.wikipedia.org/wiki/Uniform_Resource_Identifier}} (URI).
	\item[file] is the string-representation of file path.
\end{description}

An example of XML code that is specifying agent parameters is:
\begin{lstlisting}[language=XML]
<agentParameters>
	<double>1.5</double>
	<uuid>a709d65f-d33e-4314-ba76-aa7e5829f7e0</uuid>
	<string>this is a string of characters</string>
</agentParameters>
\end{lstlisting}



\section{Fog Zones}

In the context of this project, fog must be included into the simulated environment.
In order to create the fog zones, the \code{fogZones} node must be specified:
\begin{lstlisting}[language=XML]
<!ELEMENT fogZones (fogZone*)>
\end{lstlisting}

Each fog zone is assumed to be a circle on the map.
It is described in a \code{fogZone} node:
\begin{lstlisting}[language=XML]
<!ELEMENT fogZone #EMPTY>
<!ATTLIST fogZone
          enable     CDATA #IMPLIED "true"
          id         CDATA #IMPLIED
          x          CDATA #IMPLIED "0.0"
          y          CDATA #IMPLIED "0.0"
          radius     CDATA #IMPLIED
          visibility CDATA #IMPLIED
          class      CDATA #IMPLIED>
\end{lstlisting}

The \code{fogZone} node has the following attributes:
\begin{description}
	\item[enable] it indicates if the fog zone is enabled (if the value is \code{true}) or disabled (if the value is \code{false}). If the fog zone is disabled, the zone will be added to the simulated environment (\code{enable} $\in \{true, false\}$).
	\item[id] is the identifier of the fog zone. It is not a valid UUID, a valid UUID will be automatically generated from the string of characters that is specified into this attribute.
	\item[x] the X coordinate of the position of the fog zone' circle center on the map. The coordinate system is the one of the geographical information system related to the map.
	\item[y] the Y coordinate of the position of the fog zone's circle center on the map. The coordinate system is the one of the geographical information system related to the map.
	\item[radius] the radius in meters of the fog zone's circle on the map (\code{radius} $\ge 0$).
	\item[visibility] is the distance of visibility into the fog. Any environment object that has a distance greater than or equal to the \code{visibility} cannot be perceived by an agent (\code{visibility} $\ge 0$). 
	\item[class] fully qualified name of the SARL/Java class that must be used for creating the fog zone object into the simulator. The given type must be a subtype of \code{fr.ciadlab.fogsimu.environment.FogZone}.
\end{description}


\end{graphicspathcontext}

