\chapter{Environment Domain}\label{section:environmentdomain}

\begin{graphicspathcontext}{{./img/architecture/},\old}

\section{Environment model}\label{section:environmentmodel}

The \texttt{Environment Model}\index{Environment Model} that is described into the previous section contains the objects within the simulated universe, and provides the accessing functions to these objects.

\subsection{Map and roads}\label{section:gis}

Basically, traffic simulation is based on a map\index{Environment Model!Geographical Information System!Map} of roads\index{Environment Model!Geographical Information System!Road} on which vehicles are moving.
In this section, we present the concepts used to build the geographic objects of the map environment.
The presented model is implemented into the Arakhn\^e Foundation Classes\footnote{\url{htt://www.arakhne.org/afc}}\index{Arakhn\^e Foundation Classes}.
This library is used as part of the simulator implementation (see Chapter \ref{section:simulatorimplementation}).

Geographical Information Systems\index{Environment Model!Geographical Information System} (GIS) are software designed to store geo-referenced data in dedicated databases.
Basically, they organize their map\index{Environment Model!Geographical Information System!Map} data in a collection of layers.
Each layer\index{Environment Model!Geographical Information System!Layer} contains a type of data: layers, roads\index{Environment Model!Geographical Information System!Road}, public buildings\index{Environment Model!Geographical Information System!Buildings}, schools, population density areas\index{Environment Model!Geographical Information System!Density Area}, etc.
Each data in a layer\index{Environment Model!Geographical Information System!Layer} is a geo-localized shape: the map element\index{Environment Model!Geographical Information System!Map element}.
As illustrated by the UML class diagram in Figure \figref{umlgisgeneral} map elements may be lines\index{Environment Model!Geographical Information System!Map element!Line}, polygons\index{Environment Model!Geographical Information System!Map element!Polygon}, or points\index{Environment Model!Geographical Information System!Map element!Point}.

\mfigure{width=.9\linewidth}{Archi_GIS}{UML class diagram of the GIS concepts}{umlgisgeneral}

All the objects in a GIS data-structure may contain a collection of attributes for all the information associated to these GIS objects: name, element’s type, width, length, etc.

\mfigure{width=.45\linewidth}{Archi_Roads}{UML class diagram of the roads and the road system}{umlgisroads}

Roads\index{Environment Model!Geographical Information System!Road} constitute a specifically subset of the GIS objects.
They are lines\index{Environment Model!Geographical Information System!Map element!Line}, named road segments\index{Environment Model!Geographical Information System!Road}, connected to other road segments.
Figure \figref{umlgisroads} shows the UML class diagram of the road model.
Each road segment\index{Environment Model!Geographical Information System!Road} is connected to other road segments through road connection\index{Environment Model!Geographical Information System!Map element!Road connection}: cross road, etc.
Road segments\index{Environment Model!Geographical Information System!Road} and connections\index{Environment Model!Geographical Information System!Road connection} are gathering into a common structure: the road network\index{Environment Model!Geographical Information System!Road}, put inside a dedicated GIS layer\index{Environment Model!Geographical Information System!Layer}.

Because the road network\index{Environment Model!Geographical Information System!Road} is a central and very used data-structure in any GIS-based software, its implementation must be efficient to retrieve the roads and their associated attributes as fast as possible.
The simulator provides a dedicated spatial quadtree\index{Environment Model!Geographical Information System!Quadtree}.
Trees are well-known to improve software performances.
Here the tree cuts the map in four parts, and cuts each of them in four subparts, and so on.
Each road connection\index{Environment Model!Geographical Information System!Road connection} is located inside a leaf of this tree and may be retrieved with a complexity of $O(\log n)$, where $n$ is the number of road connections in the road network.

\subsection{Objects in the simulated environment} \label{section:environmentobjects}

\mfigure{width=.9\linewidth}{Archi_EnvironmentModel}{UML class diagram of the concepts that are defined into the environment model}{umlenvironmentmodel}

Figure \figref{umlenvironmentmodel} shows up the UML class diagram of the concepts that represent the objects in the simulated universe\index{Environment Model!Simulated universe}. The defined concepts are:
\begin{description}
	\item[RoadObject] is an interface that is representing all the objects within the simulated universe.
		It provides general functions for accessing to the unique identifier of the object, its position on the roads, and its shape.
	\item[AbstractNoMapElementRoadObject] is the abstract class that represents any object that is not a component of the simulated map.
	\item[AbstractMapPointSideRoadObject] is the abstract class that represents any point object on a map that may located on the road itself or on its side.
	\item[AbstractMapPointRoadObject] is the abstract class that represents any object on a road.
	\item[MobileRoadObject] is the class that represents an object able to move on a road.
	\item[MobileAgentObject] is the class that represents the bodies of the agents that are able to move along a road.
	\item[SpeedLimitPanel] represents a road sign for speed limit. If this panel is smart, it is a kind of agent body controlled by an agent. Then these agents are able to interact together.
	\item[FogWall] represents the fog. It has not a position on the map, but a distance between the agent and the fog ``wall'' could be computed.
\end{description}

\section{Creation of agent's bodies and association to agents}

According to \citet{Okamoto09}, the agent spawning\index{Spawning} is the creation of a new agent to handle part of the workload of the spawning agent.
In the context of an agent-based simulation with environment model, the spawning of an agent $a$ corresponds to:
\begin{itemize}
	\item the creation of a body $b$ that corresponds to the expected type of agent to be simulated, and
	\item the creation of the agent $a$, and association of $a$ to $b$.
\end{itemize}

The software tool that is spawning the agents is usually called a \emph{spawner}\index{Spawning!Spawner}. A specific part of the simulator's API is defined and shown on Figure \figref{umlclassspawners} up.

\mfigure{width=.7\linewidth}{Archi_Spawner}{UML class diagram that represents the spawners' definition}{umlclassspawners}

A spawner is able to create an agent body (\code{createBody} function) of the type returned by the \code{getBodyType} function.
It also provides the expected type of agent (\code{getAgentType} function) that should be associated to the body.

Spawning agents depends on two major parameters:
\begin{description}
	\item[Stochastic law] The spawner generates agents according to a given probability. This probability is computed based on a stochastic law. Several stochastic laws are defined:
		\begin{inlineenumeration}
			\item Bernoulli,
			\item Cauchi,
			\item constant,
			\item exponential,
			\item gaussian,
			\item linear,
			\item logistic,
			\item log normal,
			\item Pareto,
			\item triangular, and
			\item uniform.
		\end{inlineenumeration}
		Details on the definition of these laws are in Appendix \ref{section:nonuniformrandom}.
		The number of agents that are spawned at a given simulation time is a random number that is selected according to the law.
	\item[Budget] The spawner is able to create a maximum number of agents.
	This maximum value is called the budget.
	When the number of spawn agents has reached the budget, the spawner stops to generate agents and bodies.
\end{description}

\mfigure{width=\linewidth}{Archi_SpawningProcess}{UML sequence diagram that describes the standard process for spawning agents}{umlseqspawning}

The following functions are defined for supporting the spawning process:
\begin{description}
	\item[spawnableAt(time : TimeManager) : int] This function is invoked by the simulator in order to compute the number of agents to be created at the given time.
	\item[spawned(nb : int)] It is invoked by the simulator after the given number of agents is spawned.
\end{description}
These functions are invoked into the standard spawning process, as shown in Figure \figref{umlseqspawning}.

\section{Perceptions of the agents}

The perception process computes the set of perceived objects for each agent.

\begin{lstlisting}[caption={SARL program that computes the perception for each agent body}, label={algo:perceptionalgorithm}]
def computePerceptions {
  val perceptionBuilder = createPerceptionBuilder
  for (body : getAgentBodies) {
    perceptionBuilder.computeAndSetAgentPerception(body)
  }
}

def computeAndSetAgentPerception(perceptionBuilder : PerceptionBuilder, body : MobileAgentBody) {
  var position = body.roadPosition
  var network = this.roads.roadNetwork
  var segment = position.segment as RoadSegment
  var entryPoint = body.getRoadEntry
  var reverseOrder = (segment.endPoint == entryPoint)

  var cposition = if (reverseOrder)
      segment.length - position.curvilineCoordinate
    else
      position.curvilineCoordinate
  var distance = body.getForwardPerceptionDistance
  var iterator = network.depthIterator( (*@\label{algo:line:graphit}@*)
      segment, distance, cposition, entryPoint, false, false, createDynamicPerceptionDepthUpdater)
  var subNetwork = new SubRoadNetwork)
      perceptionBuilder.reset(body.UUID, body.forwardPerceptionDistance, body.bounds) (*@\label{algo:line:graphitend}@*)
  subNetwork.build(iterator, perceptionBuilder) (*@\label{algo:line:computeperception}@*)
  body.perceivedRoads = subNetwork (*@\label{algo:line:setperception1}@*)
  body.perceivedObjects = perceptionBuilder.getPerceptions (*@\label{algo:line:setperception2}@*)
}
\end{lstlisting}

The perception of the agents extracts data from the data structure that is containing the environment objects.
In the context of this project, the data structure is a graph of road segments (see Section \ref{section:gis}).
The field of view of an agent is a sub-graph of road segments that is restricted to the perception distance of the agent.
From line \ref{algo:line:graphit} to line \ref{algo:line:graphitend} of Algorithm \ref{algo:perceptionalgorithm}, the sub-graph is built according to the perception attributes of the agent body.
Line \ref{algo:line:computeperception} iterates on the road segments into the sub-graph and extracts the objects (vehicles, road signs, etc.)
The object \code{perceptionBuilder} is the software tool that receives the extracted objects.
Lines \ref{algo:line:setperception1} and \ref{algo:line:setperception2} sets the agent's perception with the perceived roads and the perceived objects, respectively.

\section{Influences from the agents}

Ferber's model \citep{Ferber1999MultiAgent} and its extension \citep{michel2007irm4s, michel2006modele} are based on three main principles: respect the environment integrity, agent autonomy, and clearly distinct agent's mind from its body.
In other words, an agent should not directly modify the environment, and one agent may not decide for the others.
In this theory, an agent does not perform actions but produces influences.
Influences do not directly modify the environment and, from an agent point of view, nothing can be guaranteed about their result.
Applying this theory at implementation level thus requires a two phases mechanism that firstly collects influences: \emph{Influence phase}, and then computes results of their combination: \emph{Reaction phase}.
Reaction, which is managed by the environment itself, modifies the state of the environment by combining the influences of all agents according to a previous local state of the environment and the laws that govern the simulated world.
This clear distinction between the products of the agent's behaviors and environmental reactions provides a way to handler simultaneous action in MAS and contributes to a better respect of modeling and simulation relations \citep{Zeigler.00}.

In this project, the agent's influences are collected by the \texttt{Environment Agent}.
When all the influences are received, the \texttt{Environment Agent} executes an algorithm for detecting conflicts among the influences, and solving them.

\begin{lstlisting}[caption={SARL program that applies the agents' influences to the environment state}, label={algo:influencealgorithm}]
def applyInfluences(timeManager : TimeManager) {
  for (body : getAgentBodies) { (*@\label{algo:line:influenceloop}@*)
    body.applyInfluence(body.influence, timeManager)
  }
}

def applyInfluence(body : MobileAgentBody, influence : Influence, timeManager : TimeManager) {
  if (influence instanceof PopulationInfluence) { (*@\label{algo:line:populationinfluence}@*)
    if (influence instanceof AgentBodyDestructionInfluence) { (*@\label{algo:line:bodydestruction}@*)
      var segment = body.position.segment as RoadSegment
      this.agentBodies.remove(body)
      segment.removeUserData(MOBILE_OBJECTS_ATTRIBUTE_NAME, body)
    }
  } else if (influence instanceof MotionInfluence) { (*@\label{algo:line:motioninfluence}@*)
    var move = influence.computeSteeringTransformation(body, timeManager) (*@\label{algo:line:steeringmotion}@*)
    var previousSegment = body.roadSegment
    body.transform(move, timeManager) (*@\label{algo:line:bodymove}@*)
    var currentSegment = body.roadSegment (*@\label{algo:line:segmentupdate}@*)
    if (currentSegment != previousSegment) {
      previousSegment.removeUserData(MOBILE_OBJECTS_ATTRIBUTE_NAME, body)
      currentSegment.addUserData(MOBILE_OBJECTS_ATTRIBUTE_NAME, body)
    }
  }
}
\end{lstlisting}

Line \ref{algo:line:influenceloop} of Algorithm \ref{algo:influencealgorithm} is the loop on all the agents' bodies for applying their respective influences.

Two major types of influences are supported:
The influences of type  \code{PopulationInfluence} (Line \ref{algo:line:populationinfluence}) represents the update of the agents' population. One example of update of the population is the destruction of an agent body that is requested by its owning agent (Line \ref{algo:line:bodydestruction}).
The second major type of influence is \code{MotionInfluence}.
It corresponds to the request from an agent to move its body (Line \ref{algo:line:motioninfluence}).
At Line \ref{algo:line:steeringmotion} a steering motion for the agent is computed.
A steering motion is the motion distance of the agent along a road path according to the current speed of the body and the requested acceleration from the agent.
At Line \ref{algo:line:bodymove}, the body is moved.
At Line \ref{algo:line:segmentupdate}, the link between the agent body and the road segments are updated.

\section{Simulation Lifecycle and time management}\label{section:simulationlifecycle}

The simulation framework is in charge of managing the simulation schedule, i.e. specify when the perceptions are computed and emitted, when the influences are collected and analyzed, and when the time is evolving.
In the present architecture, the definition and application of such strategies is part of the \texttt{Simulation Controller}.
The choice of the usage of a specific simulation scheduling strategy depends on the environment, and the goal of the simulation.

\mfigure{width=\linewidth}{Archi_SimulationLoop}{UML sequence diagram that is representing the general execution loop of the simulator}{simulationloop}

The simulator follows the algorithm, represented by Figure \figref{simulationloop}, in order to execute all the components of the simulator.

\end{graphicspathcontext}

