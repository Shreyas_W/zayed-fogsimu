\subsection{Log-Normal Distribution}\label{section:stochasticlaw:lognormal}\index{Non-uniform random variate generation!Inverse method!Log-normal distribution}

The log-normal distribution is the probability distribution of any random variable whose logarithm is normally distributed. If $X$ is a random variable with a normal distribution, then $\e^X$ has a log-normal distribution; likewise, if $Y$ is log-normally distributed, then $\ln(Y)$ is normally distributed.

A variable might be modeled as log-normal if it can be thought of as the multiplicative product of many small independent factors. For example the long-term return rate on a stock investment can be considered to be the product of the daily return rates.

The log-normal distribution $f(x)$ is defined by, where $x > 0$, $\mu$ is the mean, and $\sigma > 0$ is the standard deviation of the variable's logarithm (by definition, the variable's logarithm is normally distributed)\index{Non-uniform random variate generation!Log-normal distribution!Probability distribution $f(x)$}:
\begin{equation}
	f(x) = \frac{\e^{-(\ln x - \mu)^2/(2\sigma^2)}}{x \sigma \sqrt{2 \pi}}
\end{equation}

The figure~\figref{nonuniform:f:lognormal} illustrates the distribution function $f(x)$ and the cumulative distribution function $D(x)$\index{Non-uniform random variate generation!Log-normal distribution!Cumulative distribution $D(x)$}.

\mfigurewtex{.6\linewidth}{f_lognormal}{Log-normal distribution $\prob(X = x)$ and its associated cumulative function $\prob(X \le x)$}{nonuniform:f:lognormal}

According to equation~\ref{eq:random:deriveeP}, the cumulative distribution function is defined by:
\begin{equation}\label{eq:random:lognormald}\index{Non-uniform random variate generation!Log-normal distribution!Cumulative distribution $D(x)$}
	D(x) = \frac{1}{2} \left[ 1 + \erf\left(\frac{\ln(x)-\mu}{\sigma\sqrt{2}}\right)\right] 
\end{equation}

The inverse function $D^{-1}(u)$ of $D(x)$ is given by the equation~\ref{eq:random:lognormal:invd}.
\begin{equation}\label{eq:random:lognormal:invd}\index{Non-uniform random variate generation!Log-normal distribution!$D^{-1}(x)$}
	D^{-1}(u) = \e^{\sigma\sqrt{2}\erf^{-1}(2 u - 1) + \mu}
\end{equation}

An efficient implementation of the computation of $(\sigma\sqrt{2}\erf^{-1}(2 u - 1) + \mu)$ (used for the normal distribution, section~\vref{section:random:normal}) is the \emph{polar method} proposed by \makename{G.E.P.}{Box}, \makename{M.E.}{Muller}, and \makename{G.}{Marsaglia}~\cite{Box.aocp97}. This algorithm generates two independent values at the cost of only one call to the functions $\ln(x)$ and $\sqrt{x}$.

\subsubsection{Experimental Results}

The log-normal distribution was used to generate 500000 random numbers. The computation time has a means of $0.0027 ms$ during all the benchmarks\reffootnote*{bench:description}. The figure~\figref{nonuniform:lognormal:results} illustrates the density for each generated numbers.

\mfigurewtex{.6\linewidth}{test_lognormal}{Experimental Results: three log-normal distributions on 500000 random numbers}{nonuniform:lognormal:results}
