\subsection{Linear Distribution}\label{section:stochasticlaw:linear}\index{Non-uniform random variate generation!Inverse method!Linear distribution}

The linear distribution function $f(x)$ is defined by $f(x) = a x+b$\index{Non-uniform random variate generation!Linear distribution!Probability distribution $f(x)$} where, giving two constants $\xmin$ and $\xmax$:
\begin{equation}
	\begin{cases}
	\xmin \le x \le \xmax & \forall x \\
	f(\xmin) = 0 & \text{ if }a>0 \\
	f(\xmax) = 0 & \text{ if }a<0 \\
	\end{cases}
\end{equation}

The figure~\figref{nonuniform:f:linear} illustrates the distribution function $f(x)$ and the cumulative distribution function $D(x)$\index{Non-uniform random variate generation!Linear distribution!Cumulative distribution $D(x)$}.

\mfigurewtex{.6\linewidth}{f_linear}{Linear distribution $\prob(X = x)$ and its associated cumulative function $\prob(X \le x)$}{nonuniform:f:linear}

\subsubsection{Increasing Distribution Function}

Assuming that the linear distribution strictly increases on $[\xmin;\xmax]$, the distribution function is refined as $f(x) = a x - a \xmin$ according to $b = -a \xmin$\index{Non-uniform random variate generation!Linear distribution!Probability distribution $f(x)$}.

According to equation~\ref{eq:random:deriveeP}, the cumulative distribution function is defined by, where $c$ is a constant and $a$ is the constant appearing in $f(x)$:
\begin{equation}\label{eq:random:lineard}\index{Non-uniform random variate generation!Linear distribution!Cumulative distribution $D(x)$}
	D(x) = \frac{a}{2} x^2 - a \xmin x + c
\end{equation}

From the equations~\ref{eq:random:d_constraint} and~\ref{eq:random:lineard} the free-variables $a$ and $c$ could be solved:
\begin{equation}
	\begin{cases}
	D(\xmin) = \frac{a}{2} \xmin^2 - a \xmin \xmin + c = 0 \\
	D(\xmax) = \frac{a}{2} \xmax^2 - a \xmin \xmax + c = 1
	\end{cases}
	%\]
	% Demonstration:
	% \[\begin{cases}
	% 	\left(\frac{\xmin^2}{2} - \xmin^2\right) a + c = 0 \\
	% 	\left(\frac{\xmax^2}{2} - \xmin \xmax\right) a + c = 1
	%   \end{cases}\]
	% \[\begin{cases}
	% 	-\frac{\xmin^2}{2} a + c = 0 \\
	% 	\frac{\xmax^2 - 2 \xmin \xmax}{2} a + c = 1
	%   \end{cases}\]
	% \[\begin{cases}
	% 	c = \frac{\xmin^2}{2} a \\
	% 	\frac{\xmax^2 - 2 \xmin \xmax}{2} a + \frac{\xmin^2}{2} a = 1
	%   \end{cases}\]
	% \[\begin{cases}
	% 	c = \frac{\xmin^2}{2} a \\
	% 	\frac{\xmax^2 - 2 \xmin \xmax + \xmin^2}{2} a = 1
	%   \end{cases}\]
	% \[\begin{cases}
	% 	c = \frac{\xmin^2}{2} a \\
	% 	a = \frac{2}{\xmax^2 - 2 \xmin \xmax + \xmin^2}
	%   \end{cases}\]
	%\[
	\begin{cases}
	c = \frac{\xmin^2}{\xmax^2 - 2 \xmin \xmax + \xmin^2} \\
	a = \frac{2}{\xmax^2 - 2 \xmin \xmax + \xmin^2}
	\end{cases}
\end{equation}

The final equation of the cumulative distribution function $D(x)$ is defined by the equation~\ref{eq:random:lineardx}:
\begin{equation}\label{eq:random:lineardx}\index{Non-uniform random variate generation!Linear distribution!Cumulative distribution $D(x)$}
\begin{split}
   D(x) = & \frac{1}{\xmax^2 - 2 \xmin \xmax + \xmin^2} x^2 - \\
	  & \frac{2 \xmin}{\xmax^2 - 2 \xmin \xmax + \xmin^2} x + \\
	  & \frac{\xmin^2}{\xmax^2 - 2 \xmin \xmax + \xmin^2} \\
	= & \frac{(x-\xmin)^2}{(\xmax-\xmin)^2} \\
\end{split}
\end{equation}

The inverse function $D^{-1}(u)$ of $D(x)$ is given by the equation~\ref{eq:random:linear:invd}.
% \[ u = \frac{x^2 - 2 \xmin x + \xmin^2}{\xmax^2 - 2 \xmin \xmax + \xmin^2} \]
% \[ x^2 - 2 \xmin x + \xmin^2 = \left(\xmax^2 - 2 \xmin \xmax + \xmin^2\right) u \]
% \[ x^2 - 2 \xmin x = \left(\xmax^2 - 2 \xmin \xmax + \xmin^2\right) u - \xmin^2 \]
% \[ x^2 - 2 \xmin x + \xmin^2 - \left(\xmax^2 - 2 \xmin \xmax + \xmin^2\right) u  = 0\]
% \[ (x - \xmin)^2 - u (\xmax - \xmin)^2 = 0\]
% \[ (x - \xmin)^2 = (\xmax - \xmin)^2 u \]
% \[ x - \xmin = (\xmax - \xmin) \sqrt{u} \]
% \[ x = (\xmax - \xmin) \sqrt{u} + \xmin \]
\begin{equation}\label{eq:random:linear:invd}\index{Non-uniform random variate generation!Linear distribution!$D^{-1}(x)$}
D^{-1}(u) = (\xmax - \xmin) \sqrt{u} + \xmin
\end{equation}

\subsubsection{Decreasing Distribution Function}

Assuming that the linear distribution strictly decreases on $[\xmin;\xmax]$, the distribution function is refined as $f(x) = a x - a \xmax$ according to $b = -a \xmax$\index{Non-uniform random variate generation!Linear distribution!Probability distribution $f(x)$}.

According to equation~\ref{eq:random:deriveeP}, the cumulative distribution function is defined by, where $c$ is a constant and $a$ is the constant appearing in $f(x)$:
\begin{equation}\label{eq:random:lineardx2}\index{Non-uniform random variate generation!Linear distribution!Cumulative distribution $D(x)$}
	D(x) = \frac{a}{2} x^2 - a \xmax x + c
\end{equation}

From the equations~\ref{eq:random:d_constraint} and~\ref{eq:random:lineardx2} the free-variables $a$ and $c$ could be solved:
\begin{equation}
\begin{cases}
D(\xmin) = \frac{a}{2} \xmin^2 - a \xmax \xmin + c = 1  \\
D(\xmax) = \frac{a}{2} \xmax^2 - a \xmax \xmax + c = 0
\end{cases}
% \]
% % Demonstration:
% \[\begin{cases}
% 	\left(\frac{\xmin^2}{2} - \xmax \xmin\right) a + c = 1 \\
% 	\left(\frac{\xmax^2}{2} - \xmax^2\right) a + c = 0
%   \end{cases}\]
% \[\begin{cases}
% 	\frac{\xmin^2 - 2 \xmax \xmin}{2} a + c = 1 \\
% 	-\frac{\xmax^2}{2} a + c = 0
%   \end{cases}\]
% \[\begin{cases}
% 	\frac{\xmin^2 - 2 \xmax \xmin}{2} a + \frac{\xmax^2}{2} a = 1 \\
% 	c = \frac{\xmax^2}{2} a
%   \end{cases}\]
% \[\begin{cases}
% 	\frac{\xmin^2 - 2 \xmax \xmin + \xmax^2}{2} a = 1 \\
% 	c = \frac{\xmax^2}{2} a
%   \end{cases}\]
% \[\begin{cases}
% 	a = \frac{2}{\xmin^2 - 2 \xmax \xmin + \xmax^2} \\
% 	c = \frac{\xmax^2}{2} a
%   \end{cases}\]
% \[
\begin{cases}
a = \frac{2}{\xmin^2 - 2 \xmax \xmin + \xmax^2} \\
c = \frac{\xmax^2}{\xmin^2 - 2 \xmax \xmin + \xmax^2}
\end{cases}
\end{equation}

The final equation of the cumulative distribution function $D(x)$ is defined by the equation~\ref{eq:random:lineardxx}:
\begin{equation}\label{eq:random:lineardxx}\index{Non-uniform random variate generation!Linear distribution!Cumulative distribution $D(x)$}
\begin{split}
   D(x) = & \frac{1}{\xmin^2 - 2 \xmax \xmin + \xmax^2} x^2 - \\
	  & \frac{2 \xmax}{\xmin^2 - 2 \xmax \xmin + \xmax^2} x + \\
	  & \frac{\xmax^2}{\xmin^2 - 2 \xmax \xmin + \xmax^2} \\
	= & \frac{(x-\xmax)^2}{(\xmin-\xmax)^2} \\
\end{split}
\end{equation}

The inverse function $D^{-1}(u)$ of $D(x)$ is given by the equation~\ref{eq:random:linear:invdx}.
% \[ u = \frac{x^2 - 2 \xmax x + \xmax^2}{\xmin^2 - 2 \xmax \xmin + \xmax^2} \]
% \[ x^2 - 2 \xmax x + \xmax^2 = \left(\xmin^2 - 2 \xmax \xmin + \xmax^2\right) u \]
% \[ x^2 - 2 \xmax x = \left(\xmin^2 - 2 \xmax \xmin + \xmax^2\right) u - \xmax^2 \]
% \[ x^2 - 2 \xmax x + \xmax^2 - \left(\xmin^2 - 2 \xmax \xmin + \xmax^2\right) u  = 0\]
% \[ (x - \xmax)^2 - u (\xmin - \xmax)^2 = 0\]
% \[ (x - \xmax)^2 = (\xmin - \xmax)^2 u \]
% \[ x - \xmax = (\xmin - \xmax) \sqrt{u} \]
% \[ x = (\xmin - \xmax) \sqrt{u} + \xmax \]
\begin{equation}\label{eq:random:linear:invdx}\index{Non-uniform random variate generation!Linear distribution!$D^{-1}(x)$}
D^{-1}(u) = (\xmin - \xmax) \sqrt{u} + \xmax
\end{equation}

\subsubsection{Experimental Results}

The linear distribution was used to generate 500000 random numbers. The computation time has a means of $0.0023 ms$ during all the benchmarks\reffootnote*{bench:description}. The figure~\figref{nonuniform:linear:results} illustrates the density for each generated number. The three test suites highlight the attempted distribution along the value intervals:
\begin{inlineenumeration}
 \item inscreasing distribution in $[2;8]$;
\item inscreasing distribution in $[-7;0]$.
\item decreasing distribution in $[3;7]$;
\end{inlineenumeration}
In figure~\figref{nonuniform:linear:results} each point represents the generated values truncated at the 3\urd decimal.

\mfigurewtex{.6\linewidth}{test_linear}{Experimental Results: of three Linear Distributions on 500000 random numbers}{nonuniform:linear:results}
