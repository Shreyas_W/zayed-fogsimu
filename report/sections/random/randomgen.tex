\chapter{Non-Uniform Random Number Generation}\label{section:nonuniformrandom}

\savefootnote*{The bench was proceeded on Intel\regmark Core\trademark 2 T5500 processor at 1.66GHz with 1GB RAM. The operating system is Linux 2.6.20 and the Java virtual machine has the version 1.6\_beta\_2. }{bench:description}

\begin{graphicspathcontext}{{./img/random/},\old}

Non-uniform random variate generation\index{Non-uniform random variate generation!Principle} is concerned with the generation of random variables with certain distributions. Such random variables are often discrete, taking values in a countable set, or absolutely continuous, and thus described by a density. The methods used for generating them depend upon the computational model one is working with, and upon the demands on the part of the output~\cite{Devroye.book86,LEcuyer.simulation98,Ritabrata.rand02,Tirler.dsc03,Hormann.book04}.

The basic building block for generating random numbers from various distributions is a generator of uniform random numbers on the interval $[0;1]$\index{Uniform random variate generation}. The corresponding independent identically distributed sequence of variables is called $U(0,1)$. In a mathematical point of view designing a non-uniform random variate based on it is unrealistic. But it has several advantages to proceed on this way:
\begin{inlineenumeration}
\item it allows to disconnect the theory of non-uniform random number generation from that of the uniform random number generation;
\item it permits to plan the future evolution of the finite approximation methods which are currently limited by the computers' computation power.
\end{inlineenumeration}

In this chapter, the generation of uniform random numbers is presented in the section~\ref{section:nonuniformrandom:uniform}. The section~\ref{section:nonuniformrandom:invertmethod} describes the inverse method used for generating non-uniform random numbers.

\section{Uniform Random Number Generation}\label{section:nonuniformrandom:uniform}\index{Uniform random variate generation!Principle}

Due to the finite precision of computers, a true continuous random variate could not be generated. However a discrete random variate very close to $U(0,1)$ is generated.

There is a very rich literature on the generation of random integers, commonly called pseudorandom numbers because they are actually deterministic~\cite{Greenberger.acm61,LEcuyer.aor94,Knuth.aocp97}. Pseudorandom numbers can be divided by their upper bound to generate $U(0;1)$ variates as illustrated by Algorithm~\ref{algo:uniformrandomnumber}\index{Uniform random variate generation!Algorithm}.

\begin{algorithm}
	\begin{algorithmic}
		\State $C \gets 25173$
		\State $D \gets 13849$
		\State $M \gets 32768$
		\State $Seed$ \Comment{must be initialized with the desired initial seed value}
		\Function{random}{}
		\State $Seed \gets (C \times Seed + D) \mod M$
		\State \textbf{return} $Seed / M$
		\EndFunction
	\end{algorithmic}
	\caption{Uniform Random Number Generation}
	\label{algo:uniformrandomnumber}
	\index{Uniform random variate generation!Seed}\index{Uniform random variate generation!Algorithm}
\end{algorithm}

The name $Seed$\index{Uniform random variate generation!Seed} stems from the fact that most random number libraries ask the user to specify what they call the seed value. The value the user gives ``seeds'' the sequence of random numbers that are generated. The $random$ function is supposed to give values in $[0;1]$. The approximation quality of $U(0,1)$ variate generate is a key point. If $X_i$ is the value replied by the $random$ function at its $i$\uth call, the value $X_i$ should be independent. The equations~\ref{eq:uniform:independent:a} and~\ref{eq:uniform:independent:b} describe the properties that must be verified to allow the variable indenpendence. In equation~\ref{eq:uniform:independent:a} $X_i$ are defined to be uniformly distributed on $[0;1]$ and in equation~\ref{eq:uniform:independent:b}, and its higher-dimensional versions, $X_i$ variates are defined to be independent.

\begin{equation}\label{eq:uniform:independent:a}\index{Uniform random variate generation!Properties}
\begin{split}
 \forall r,s / 0 < r < s < 1 \\
 \lim_{n\rightarrow\inf}\frac{C(r,s,n)}{n} = s - r
\end{split}
\end{equation}
where $C(r,s,n)$ is a count of the number of $X_i$ which fall into $(r,s)$, $i=1,\ldots,n$.

\begin{equation}\label{eq:uniform:independent:b}\index{Uniform random variate generation!Properties}
\begin{split}
 \forall r,s,u,v,k / 0 < r < s < 1 \wedge 0 < u < v < 1 \wedge k > 0 \\
 \lim_{n\rightarrow\inf}\frac{D(r,s,u,v,n,k)}{n} = (s - r)(u-v)
\end{split}
\end{equation}
where $D(r,s,u,v,n,k)$ is a count of the number of $X_i$ for which $r<X_i<s$ and $u<X_{i+k}<v$, $i=1,\ldots,n$. The third and higher dimensional versions of the equation~\ref{eq:uniform:independent:b} hold.

To satisfy the property expressed by the equation~\ref{eq:uniform:independent:a}, the value of $Seed$\index{Uniform random variate generation!Seed} must be taken in the set $\{0 \ldots M-1\}$ without repetition until all the elements of the set was selected. This condition is true if\index{Uniform random variate generation!Properties}:
\begin{inlineenumeration}
 \item $D$ and $M$ are relatively prime;
 \item every prime factor of $M$ is a divider of $C-1$;
 \item $C-1$ must be divisible by 4 if $M$ is divisible by 4.
\end{inlineenumeration}

Basically $M$ is chosen to be the word size of the computer --- $2^{32}$ in Algorithm~\ref{algo:uniformrandomnumber} --- because the $\mod$ operation is more efficient to run. However, determining which values of $C$ and $D$ give approximate independence requires experiemntal investigation.

\section{Inverse Method}\label{section:nonuniformrandom:invertmethod}\index{Non-uniform random variate generation!Inverse method}

The inverse method is the basic method to generate non-uniform random numbers based on a continuous distribution.

\subsection{Principle}\index{Non-uniform random variate generation!Inverse method!Principle}

The probability distribution function $f(x) = \prob(X = x)$\index{Non-uniform random variate generation!Inverse method!Probability distribution $f(x)$} describes the probability that a variate $X$ takes on a value equal to a number $x$.

The cumulative density function $D(x)$ describes the probability that a variate $X$ takes on a value less than or equal to a number $x$. Giving a lower bound $x_{min}$ the density function is related to the distribution function $f(x)$ by:
\begin{equation}
D(x) = \prob(X \le x) = \int_{-\infty}^x f(\xi)d\xi
\end{equation}

Every cumulative distribution function $D(x)$ is (not necessarily strictly) monotone increasing and right-continuous. Furthermore, $D(x)$ has\index{Non-uniform random variate generation!Principle!Cumulative distribution $D(x)$}:
\begin{equation}\label{eq:random:d_constraint}
  \begin{cases}
	\underset{x\rightarrow-\infty}{\lim}\;D(x) = 0 \\
	\underset{x\rightarrow+\infty}{\lim}\;D(x) = 1 \\
  \end{cases}
\end{equation}

If the $D(x)$ is continuous, then $X$ is a continuous random variable; if furthermore $D(x)$ is absolutely continuous, then there exists a Lebesgue-integrable function $f(x)$ such that:
\[ D(b)-D(a) = \prob(a \leq X \leq b) = \int_a^b f(x)\,dx \]
for all real numbers $a$ and $b$. The function $f(x)$ is equal to the derivative of $D(x)$ almost everywhere, and it is called the probability density function of the distribution of $X$:
\begin{equation}\label{eq:random:deriveeP}
f(x) = D'(x)
\end{equation}

If the random number generator yields an uniformly distributed value $y_i$ in $[0,1]$ for each trial $i$, then a random number could be computed according to the distribution function $f(x)$ with: $D(x) = \int^x f(x')dx'$.

The formula connecting $y_i$ with a variable distributed as $f(x)$ is then $x_i = D^{-1}(y_i)$ where $D^{-1}(x)$ is the inverse function of $D(x)$.

In the following sections, several distribution functions are detailled.

%%------------------------------------- DISTRIBUTIONS
\input{sections/random/normaldist}
\input{sections/random/exponentialdist}
\input{sections/random/lognormaldist}
\input{sections/random/cauchydist}
%\input{sections/random/logarithmicdist}
\input{sections/random/paretodist}
\input{sections/random/logisticdist}
\input{sections/random/triangulardist}
\input{sections/random/lineardist}

\section{Procedural Method}\label{section:nonuniformrandom:proceduralmethod}\index{Non-uniform random variate generation!Procedural method}

The procedural method is one of the basic methods to generate non-uniform random numbers based on discrete distributions.
They are based on a distribution function $f$ which could be easely reversed to obtain random numbers. In this approach, most of the method are based on finite discrete distribution functions\index{Non-uniform random variate generation!Procedural method!Principle}.

%%------------------------------------- DISTRIBUTIONS
\input{sections/random/bernoullidist}

\end{graphicspathcontext}

