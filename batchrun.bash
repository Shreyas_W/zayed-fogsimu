#!/usr/bin/env bash

max=2
counter=1
while [ $counter -le $max ]
do
	echo "****************** $counter"
	/usr/lib/jvm/java-8-oracle-amd64/bin/java -jar ./target/fogsimu-0.1-SNAPSHOT-cli.jar --noui "$1"
	((counter++))
done

